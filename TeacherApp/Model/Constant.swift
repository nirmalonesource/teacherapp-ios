//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()

}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0

}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}

//MARK:- Webservices

   /*
   Confirm OTP
   */

 struct ServiceList
 {
   // static let SERVICE_URL = "https://test.childrens.parshvaa.com/api/v2/"
    static let SERVICE_URL = "https://cagssmart.com/api/v1/"

    static let X_CI_CHILDRENS_ACADEMY_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"
    static let USERNAME = "CHILDRENS-ACADEMY-API"
    static let PASSWORD = "API@CHILDRENS123"
    
    static let LOGIN = "teacher/login"
    static let RESEND_OTP = "teacher/login/resendotp"
    static let CONFIRM_OTP = "teacher/login/confirmotp"
    static let QUERYMESSAGE = "querymessage"
    
    static let GET_PAPER_LIST = "teacher/paperlist"
    static let GET_PAPER_DATE_LIST = "teacher/paper_dates"
    static let ZOOKI_LIST = "zooki"
    static let LEGAL = "legal/teacher"
    static let BRANCH_BATCH = "lists/branch_batch"
    static let PUBLISH_PAPER = "teacher/publish_paper"
    static let GET_SUBJECT_LIST = "lists/subject_list"
    static let GET_PROFILE = "teacher/profile"
    static let GET_REPORT = "teacher/planner_summary"
    static let GET_SUMMARY = "teacher/Batch_summary"
    static let GET_TEACHERREPORT = "teacher/teacher_report"
    static let GET_STUDENTDETAIL = "teacher/Student_details"
    static let GET_PLANNERDETAILS = "teacher/Planner_details"
    static let GET_STUDENTSUMMARY = "teacher/Student_summary"

    static let GET_PAPER_QUESTION_LIST = "student/paperquestionlist"
    static let STUDENT_SUBMIT_TEST = "student/submit_test"
    

     static let Get_AssignPaperlist = "teacher/AssignPaperlist"

}

