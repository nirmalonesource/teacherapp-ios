//
//  extensions.swift
//  ChatApp
//
//  Created by My Mac on 2/12/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration
import MobileCoreServices
import SHSnackBarView

//MARK:- Tab + Navigation

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

extension UIViewController
{
    //Tab bar
    func addTab(strView : String)
    {
        var myCustomView: BottomTabView?
        if myCustomView == nil {
            myCustomView = Bundle.main.loadNibNamed("BottomTabView", owner: self, options: nil)?.first as? BottomTabView
            
            var height : CGFloat = 75
            if DeviceType.IS_IPHONE_X
            {
                height = 75
                //                myCustomView?.frame = CGRect(x: 0, y: self.view.frame.height - height - 34, width: ScreenSize.SCREEN_WIDTH, height: height)
                myCustomView?.frame = CGRect(x: 0, y: self.view.frame.height - height, width: ScreenSize.SCREEN_WIDTH, height: height + 40)
            }
            else
            {
                height = 55
                myCustomView?.frame = CGRect(x: 0, y: self.view.frame.height - height, width: ScreenSize.SCREEN_WIDTH, height: height)
            }
//
//            height = 55
//            myCustomView?.frame = CGRect(x: 0, y: self.view.frame.height - height, width: ScreenSize.SCREEN_WIDTH, height: height)
            
            myCustomView?.lblBGGredient.setGredient()
            myCustomView?.btnMatches.addTarget(self, action: #selector(btnMatchesTabAction), for: .touchUpInside)
            myCustomView?.btnDiscover.addTarget(self, action: #selector(btnDiscoverTabAction), for: .touchUpInside)
            myCustomView?.btnLikes.addTarget(self, action: #selector(btnLikesTabAction), for: .touchUpInside)
            myCustomView?.btnInbox.addTarget(self, action: #selector(btnInboxTabAction), for: .touchUpInside)
            
            myCustomView?.imgMatch.changeImageViewImageColor(color: UIColor.white)
            
            for view in (myCustomView?.vwMain.subviews)! {
                if let btn = view as? UIButton {
                    btn.backgroundColor = UIColor.clear
                }
            }

            if strView == "Dashboard"
            {
                myCustomView?.btnMatches.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
            }
            else if strView == "Search"
            {
                myCustomView?.btnDiscover.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
            }
            else if strView == "Report"
            {
                myCustomView?.btnLikes.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
            }
            else if strView == "Zooki"
            {
                myCustomView?.btnInbox.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
            }
            
            myCustomView?.removeFromSuperview()
            self.view.addSubview(myCustomView!)
        }
    }

    @objc func btnMatchesTabAction()
    {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    @objc func btnDiscoverTabAction(_sender : UIButton)
    {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    @objc func btnLikesTabAction()
    {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "reportViewController") as! reportViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }

    @objc func btnInboxTabAction()
    {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StoryboardViewController") as! StoryboardViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func btnProfileTabAction()
    {
        
    }
    
    // Navigation bar
    func setGlobalNavigationbarWithMenuButton(strTitle : String , strRightTitle : String , color : UIColor , strBadgeCount : String , strRightTitle2 : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().isTranslucent = true
        navigationController?.navigationBar.barTintColor = color

        self.navigationItem.hidesBackButton = true
        
//        let sizeLength = UIScreen.main.bounds.size.height * 2
//        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 44)
//
//        let bgGredientImage = UIImageView()
//        bgGredientImage.frame = defaultNavigationBarFrame
//        bgGredientImage.setGredient()
//
//        self.navigationController?.navigationBar.addSubview(bgGredientImage)

        let gradient = CAGradientLayer()
        var bounds =  self.navigationController?.navigationBar.bounds
        bounds!.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds!
//        gradient.colors = [UIColor.red.cgColor, UIColor.blue.cgColor]
        gradient.colors = [UIColor.init(rgb: ConstantVariables.Constants.navigationColor).cgColor,
         UIColor.init(rgb: ConstantVariables.Constants.lightGreen).withAlphaComponent(1).cgColor]
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 0)
        
        if let image = getImageFrom(gradientLayer: gradient) {
             self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
        
        let btnName = UIButton()
        let image = UIImage(named: "menu_icon")?.withRenderingMode(.alwaysTemplate)
        btnName.setImage(image, for: .normal)
        btnName.tintColor = UIColor.white
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.addTarget(self, action: #selector(btnMenuAction), for: .touchUpInside)
        
        let menuBarButton1 = UIBarButtonItem()
        menuBarButton1.customView = btnName
        
//        let menuBarButton2 = UIBarButtonItem()
//        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
//        lblTitle.text = strTitle
//        lblTitle.textColor = UIColor.white
//        lblTitle.textAlignment = .left
//        menuBarButton2.customView = lblTitle
        
        self.title = strTitle
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        let btnInfo = UIButton()
        btnInfo.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let image1 = UIImage(named: strRightTitle)?.withRenderingMode(.alwaysTemplate)
        btnInfo.setImage(image1, for: .normal)
        btnInfo.tintColor = UIColor.white
        btnInfo.setTitleColor(UIColor.white, for: .normal)
        btnInfo.titleLabel?.textAlignment = .right
        btnInfo.addTarget(self, action: #selector(btnRightAction), for: .touchUpInside)

        let btnFilter = UIButton()
        btnFilter.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let image2 = UIImage(named: strRightTitle2)?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(image2, for: .normal)
        btnFilter.tintColor = UIColor.white
        btnFilter.imageView?.contentMode = .scaleAspectFit
        btnFilter.addTarget(self, action: #selector(btnRightAction2), for: .touchUpInside)
        
        let menuBarRightButton1 = UIBarButtonItem()
        menuBarRightButton1.customView = btnInfo
       
        let menuBarRightButton2 = UIBarButtonItem()
        menuBarRightButton2.customView = btnFilter
        
        self.navigationItem.setLeftBarButtonItems([menuBarButton1], animated: true)
        self.navigationItem.setRightBarButtonItems([menuBarRightButton1,menuBarRightButton2 ], animated: true)
        
    }
    
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    @objc func btnMenuAction()
    {
//        menuContainerViewController.toggleLeftSideMenuCompletion(nil)
    }
    
    @objc func btnRightAction()
    {
        
    }
    
    @objc func btnRightAction2()
    {
        
    }
    
    //Back Button
    
    func setGlobalNavigationbarWithBackButton(strTitle : String , strRightTitle : String , strBadgeCount : String , color : UIColor, isBlackArrow : Bool)
    {
        navigationItem.hidesBackButton = true
        navigationController?.navigationBar.barTintColor = color
        self.navigationController?.isNavigationBarHidden = false
        UINavigationBar.appearance().isTranslucent = true
        
        let btnName = UIButton()
 
        btnName.setImage(UIImage(named: "backicon"), for: .normal)
        btnName.imageView?.changeImageViewImageColor(color: UIColor.white)
        
        btnName.setImage(btnName.imageView?.image?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnName.tintColor = color
        
        btnName.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnName.addTarget(self, action: #selector(btnGlobalBackAction), for: .touchUpInside)
        
        let menuBarButton1 = UIBarButtonItem()
        menuBarButton1.customView = btnName
        
        self.title = strTitle
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
        // Info button
        
        let label = UILabel(frame: CGRect(x: 15, y: -10, width: 20, height: 20))
        label.layer.cornerRadius = label.bounds.size.height / 2
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.init(rgb: ConstantVariables.Constants.navigationColor)
        
        if let a = UserDefaults.standard.object(forKey: "cart_count")
        {
            label.text = "\(a)"
            
            if "\(a)" == "0"
            {
                label.isHidden = true
            }
            else
            {
                label.isHidden = false
            }
        }
        else
        {
            label.text = "0"
            label.isHidden = true
        }
        
        label.font = UIFont.systemFont(ofSize: 10)
        label.layer.cornerRadius = label.frame.height/2
        label.clipsToBounds = true
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.white.cgColor
        label.isUserInteractionEnabled = false
        
        let btnInfo = UIButton()
        btnInfo.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnInfo.setImage(UIImage(named : strRightTitle), for: .normal)
        if strRightTitle != ""
        {
            let image = UIImage(named: strRightTitle)?.withRenderingMode(.alwaysTemplate)
            btnInfo.setImage(image, for: .normal)
            btnInfo.tintColor = UIColor.white
        }
        
        if strRightTitle != ""
        {
            btnInfo.addSubview(label)
        }
        
        btnInfo.addTarget(self, action: #selector(btnGlobal_Back_RightAction), for: .touchUpInside)
        
        let menuBarRightButton1 = UIBarButtonItem()
        menuBarRightButton1.customView = btnInfo
        
        if isBlackArrow
        {
            self.navigationItem.setLeftBarButtonItems([menuBarButton1], animated: true)
        }
        self.navigationItem.setRightBarButton(menuBarRightButton1, animated: true)
        
    }
    
    @objc func btnGlobalBackAction()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnGlobal_Back_RightAction()
    {
        
    }
    
    func openGalleryCameraPicker(picker : UIImagePickerController)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera(picker: picker)
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //picker.mediaTypes = [kUTTypePNG as NSString as String]
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Image Selection
    func openCamera(picker : UIImagePickerController){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
        }
        else
        {
            openGallary(picker: picker)
        }
    }
    
    func openGallary(picker : UIImagePickerController)
    {
        picker.allowsEditing = true
        self.present(picker, animated: true, completion: nil)
    }
    
    //Image_Video Selection
    
    func openImageVideoSelectionPicker(picker : UIImagePickerController)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Choose Image", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGalleryCameraPicker(picker: picker)
        }
        let gallaryAction = UIAlertAction(title: "Choose Video", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallaryVideos(picker: picker)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        //picker.mediaTypes = [kUTTypePNG as NSString as String]
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallaryVideos(picker : UIImagePickerController)
    {
        picker.sourceType = .savedPhotosAlbum
        picker.allowsEditing = true
        picker.videoMaximumDuration = TimeInterval(30.0)
        picker.mediaTypes = [kUTTypeMovie as NSString as String]
        self.present(picker, animated: true, completion: nil)
    }
    
    func showViewWithAnimation(vw : UIView , img : UIImageView) {
        vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.3) {
            vw.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            vw.isHidden = false
            img.isHidden = false
            
        }
    }
    
    func hideViewWithAnimation(vw : UIView , img : UIImageView) {
        UIView.animate(withDuration: 0.3, animations: {
            vw.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }, completion: {
            (value: Bool) in
            vw.isHidden = true
            img.isHidden = true
        })
    }
    
}

//MARK:- TextField
extension UITextField
{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat , strImage : String){
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
        paddingView.changeImageViewImageColor(color: UIColor.white)
        paddingView.contentMode = .scaleAspectFit
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat , strImage : String){
        let paddingView = UIImageView(frame: CGRect(x: 15, y: 20, width: amount, height: 20))
        paddingView.image = UIImage(named : strImage)
        paddingView.contentMode = .scaleAspectFit
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
}

//MARK:- TextView

extension UITextView
{
    func setBorder() {
        self.layer.shadowRadius = 10.0
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.black.cgColor
    }
}

//MARK:- Corner Radius

extension UIImageView
{
    func setRadius( contentmode : UIView.ContentMode ,  radius: CGFloat? = nil) {
        self.layoutIfNeeded()
        self.contentMode = contentmode
        self.layer.cornerRadius = radius ?? self.frame.height / 2
        self.layer.masksToBounds = true
        self.layoutIfNeeded()
    }
    
}

//MARK:- Image

extension UIImage
{
    func imageWithInsets(insetDimen: CGFloat) -> UIImage {
        return imageWithInsets(insets: UIEdgeInsets(top: insetDimen, left: insetDimen, bottom: insetDimen, right: insetDimen))!
    }
    
    func imageWithInsets(insets: UIEdgeInsets) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.size.width + insets.left + insets.right,
                   height: self.size.height + insets.top + insets.bottom), false, self.scale)
        let _ = UIGraphicsGetCurrentContext()
        let origin = CGPoint(x: insets.left, y: insets.top)
        self.draw(at: origin)
        let imageWithInsets = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return imageWithInsets
    }
    
}

//MARK:- Image

extension UIImageView
{
    func changeImageViewImageColor(color : UIColor)
    {
        self.image = self.image!.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
}

//MARK:- UIView

extension UIView
{
    func setRadius(radius: CGFloat) {
        self.layoutIfNeeded()
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    func setRadiusBorder(color: UIColor) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 2
        self.layer.masksToBounds = true
    }
    
    func setDarkRadiusBorder(color: UIColor , weight : CGFloat) {
        self.layoutIfNeeded()
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = weight
        self.layer.masksToBounds = true
    }
    
    func setDottedRadiusBorder(color: UIColor) {
        self.layoutIfNeeded()
        let border = CAShapeLayer()
        border.strokeColor = color.cgColor
        border.fillColor = nil
        border.lineDashPattern = [4, 4]
        border.path = UIBezierPath(rect: self.bounds).cgPath
        border.frame = self.bounds
        self.layer.addSublayer(border)
        self.layoutIfNeeded()
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        self.layoutIfNeeded()
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        self.layoutIfNeeded()
    }
    
    func setGradientBackground() {
        let colorTop =  UIColor.black.withAlphaComponent(1).cgColor
        let colorBottom = UIColor.white.withAlphaComponent(0.1).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop , colorTop, colorBottom]
        gradientLayer.locations = [0.0 , 0.05  , 1.0]
        gradientLayer.frame = self.bounds
        self.layer.mask = gradientLayer
    }
    
    func setGredientBorder()
    {
        let gradient = CAGradientLayer()
        gradient.frame =  CGRect(origin: CGPoint.zero, size: self.frame.size)
        gradient.colors = [UIColor.black.withAlphaComponent(0.7).cgColor, UIColor.lightGray.withAlphaComponent(0.5).cgColor]
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(rect: self.bounds).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradient.mask = shape
        
        self.layer.addSublayer(gradient)
    }
    
    func setGredientDark()
    {
        self.layoutIfNeeded()
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.lightGray.cgColor,
                                 UIColor.white.withAlphaComponent(1).cgColor].map{$0}
        self.layer.addSublayer(gradientLayer)
        self.layoutIfNeeded()
    }
    
    func setShadow()
    {
        self.layer.shadowColor = UIColor.init(rgb: 0xB5B5B5).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 4.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }
    
    func setTopShadow()
    {
        self.layer.shadowColor = UIColor.init(rgb: 0xB5B5B5).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: -4.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false
    }
    
    func setGredient()
    {
        self.layoutIfNeeded()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.init(rgb: ConstantVariables.Constants.lightGreen).cgColor,
                                 UIColor.init(rgb: ConstantVariables.Constants.navigationColor).withAlphaComponent(1).cgColor].map{$0}
//        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
//        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.addSublayer(gradientLayer)
    }
    
    //Table Animation
    func loadAnimation( ) {
        self.transform = CGAffineTransform(translationX: 0, y: 50)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func loadAnimationForCollection( ) {
        self.transform = CGAffineTransform(translationX:50, y: 0)
        self.alpha = 0
        
        UIView.animate(withDuration: 0.6, delay: 0.05, options: [.curveEaseInOut], animations: {
            self.transform = CGAffineTransform(translationX: 0, y: 0)
            self.alpha = 1
        }, completion: nil)
    }
    
    func dampAnimation()
    {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.4,
                       initialSpringVelocity: 8.0,
                       options: .allowUserInteraction,
                       animations: {
                        self.transform = .identity
                        self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                        
        },
                       completion: nil)
    }
    
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }) { (completed) in
            self.isHidden = true
            completion(true)
        }
    }
}

//MARK:- Color

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    var hexString:String? {
        if let components = self.cgColor.components {
            let r = components[0]
            let g = components[1]
            let b = components[2]
            return  String(format: "%02X%02X%02X", (Int)(r * 255), (Int)(g * 255), (Int)(b * 255))
        }
        return nil
    }

}


//MARK:- UIAlertController

func showMessage(msg: String , uiview: UIViewController) {
    let alert = UIAlertController(title: ConstantVariables.Constants.Project_Name, message: msg, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
    uiview.present(alert, animated: true, completion: nil)
}

//MARK:- Custom Alert
func showAlert(uiview: UIViewController , msg : String , isTwoButton : Bool) {

        let window : UIWindow = UIApplication.shared.keyWindow!
        let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: msg)

        let action1 = EMAlertAction(title: "Ok", style: .cancel)
        let action2 = EMAlertAction(title: "Cancel", style: .normal) {
            // Perform Action
        }

        alert.addAction(action: action1)
        if isTwoButton {
            alert.addAction(action: action2)
        }
        uiview.present(alert, animated: true, completion: nil)
}

//MARK:- String

extension String {
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func trimmingString() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func URLEncodedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        return escapedString
    }
    
    func URLQueryAllowedString() -> String? {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        return escapedString
    }
    
    func htmlAttributed(family: String?, size: CGFloat, color: UIColor) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "color: #\(color.hexString!) !important;" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
   
    
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            let textAttributes = [
                NSAttributedString.Key.foregroundColor: UIColor.black,
                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 14)
                ] as [NSAttributedString.Key: Any]
            
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
}

//MARK:- Custom No Internet Alert

func isInternetAvailable() -> Bool
{
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
        return false
    }
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
}

func noInternetConnectionAlert(uiview: UIViewController) {

    let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Please check your internet connection or try again later")
    let action1 = EMAlertAction(title: "Ok", style: .cancel)
    alert.addAction(action: action1)
    uiview.present(alert, animated: true, completion: nil)
}

//MARK:- Toast

func showToast(uiview: UIViewController , msg : String )
{
    let window : UIWindow = UIApplication.shared.keyWindow!
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

    let snackbarView = snackBar()
    snackbarView.showSnackBar(view: appDelegate.window!, bgColor: UIColor.black, text: msg, textColor: UIColor.white, interval: 2)
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

extension UIButton
{
    func setGredientText()
    {
        self.layoutIfNeeded()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.init(rgb: ConstantVariables.Constants.lightGreen).cgColor,
                                 UIColor.init(rgb: ConstantVariables.Constants.navigationColor).withAlphaComponent(1).cgColor].map{$0}
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.addSublayer(gradientLayer)
        
        let label = UILabel(frame: self.bounds)
        label.text = self.currentTitle
        label.font = self.titleLabel?.font
        label.textAlignment = .center
        self.addSubview(label)
        self.mask = label
    }
}

