//
//  ShareViewController.swift
//  TeacherApp
//
//  Created by My Mac on 24/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
//import ActionSheetPicker_3_0
import SwiftyPickerPopover

class ShareBatchListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRight: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class ShareViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{

    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var btnAllBranch: UIButton!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var imgShareBG: UIImageView!    
    
    var finishedLoadingInitialTableCells = false
    var batchData = [[String : Any]]()
    var MCQPlannerID = String()
    var selectedID = [String]()
    var strSelectedDate = String()

    //MARK:- Web service
    
    func getBatchList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = [
                "MCQPlannerID" : MCQPlannerID
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.BRANCH_BATCH)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.batchData.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.batchData = resData["data"] as? [[String:Any]] ?? []
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.tblVW.reloadData()
                        self.view.layoutIfNeeded()
                        self.constTblHeight.constant = self.tblVW.contentSize.height
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func publishPaper()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            var dictArr = [[String : Any]]()
            var dict = [String : Any]()
            for i in 0..<selectedID.count
            {
                dict["BatchID"] = selectedID[i]
                dictArr.append(dict)
            }
            
            var convertedString = String()
            do {
                let data1 =  try JSONSerialization.data(withJSONObject: dictArr, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                convertedString = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
                print(convertedString) // <-- here is ur string
                
            } catch let myJSONError {
                print(myJSONError)
            }
            
            let parameters = [
                "MCQPlannerID" : MCQPlannerID ,
                "BatchID" : convertedString ,
                "PublishDate" : strSelectedDate
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.PUBLISH_PAPER)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.batchData.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            for vc in (self.navigationController?.viewControllers ?? []) {
                                if vc is DashboardViewController {
                                    _ = self.navigationController?.popToViewController(vc, animated: true)
                                    break
                                }
                            }
                        }
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        btnSubmit.setRadius(radius: btnSubmit.frame.height/2)
        imgShareBG.setRadius(radius: imgShareBG.frame.height/2)
        btnSubmit.setGredientText()
        btnSubmit.dampAnimation()
        
        tblVW.setRadius(radius: 10)
        tblVW.tableFooterView = UIView()
        btnAllBranch.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        imgArrow.changeImageViewImageColor(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
        
        getBatchList()
        
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblVW.contentSize.height
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return batchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ShareBatchListCell = tableView.dequeueReusableCell(withIdentifier: "ShareBatchListCell") as! ShareBatchListCell
        
        cell.lblRight.setRadius(radius: cell.lblRight.frame.height/2)
        cell.lblRight.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
        cell.lblTitle.text = batchData[indexPath.row]["Batch"] as? String
        
        if selectedID.contains(batchData[indexPath.row]["BatchID"] as? String ?? "0")
        {
            cell.lblRight.text = "✓"
        }
        else
        {
            cell.lblRight.text = ""
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedID.contains(batchData[indexPath.row]["BatchID"] as? String ?? "0")
        {
            selectedID = selectedID.filter{$0 != batchData[indexPath.row]["BatchID"] as? String ?? "0"}
        }
        else
        {
            selectedID.append(batchData[indexPath.row]["BatchID"] as? String ?? "0")
        }
        
        tblVW.reloadData()
        let cell : ShareBatchListCell = (tblVW.cellForRow(at: indexPath) as? ShareBatchListCell)!
        cell.lblRight.dampAnimation()
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if batchData.count > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimationForCollection()
        }
    }
    
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnBrachAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        let datePicker = ActionSheetDatePicker(title: "Select Date:", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
//            picker, value, index in
//
//            let dateFormatterGet = DateFormatter()
//            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MMM dd , yyyy"
//
//            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
//            print(dateFormatter.string(from: date!))
//            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
//
//            let dateFormatter1 = DateFormatter()
//            dateFormatter1.dateFormat = "yyyy-MM-dd"
//
//            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
//            self.strSelectedDate = (dateFormatter1.string(from: date1!))
//            print("Selected : \(self.strSelectedDate)")
//            return
//        }, cancel: { ActionStringCancelBlock in return },
//           origin: sender.superview!.superview)
//        datePicker?.show()
        
         DatePickerPopover(title: "Select Date")
          .setDateMode(.date)
          //.setMaximumDate(Date())
          .setSelectedDate(Date())
          .setDoneButton(action: {
              popover, selectedDate in
              
              print("selectedDate \(selectedDate)")
              let formatter = DateFormatter()
                  formatter.dateFormat = "dd-MM-yyyy"
             sender.setTitle("\(formatter.string(from: selectedDate))", for: .normal)
            
             let dateFormatter1 = DateFormatter()
                dateFormatter1.dateFormat = "yyyy-MM-dd"
            self.strSelectedDate = (dateFormatter1.string(from: selectedDate))
                print("Selected : \(self.strSelectedDate)")
          })
          .setCancelButton(action: { _, _ in print("cancel")})
          .appear(originView: sender, baseViewController: self)
              
          
        
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        if selectedID.count > 0 && strSelectedDate != ""
        {
            publishPaper()
        }
        else if strSelectedDate == ""
        {
            showAlert(uiview: self, msg: "Please select date", isTwoButton: false)
        }
        else
        {
            showAlert(uiview: self, msg: "Please select batch", isTwoButton: false)
        }
    }
    
    
}
