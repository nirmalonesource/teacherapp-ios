//
//  TestQuestionVideoListView.swift
//  TeacherApp
//
//  Created by My Mac on 09/07/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage
import AVKit
import JMMarkSlider

class TestQuestionVideoListView: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , SubCategoryProductDelegate , UIWebViewDelegate {
    
    @IBOutlet weak var BGview: UIView!
    //    override func viewDidLayoutSubviews() {
    //        let cell:TestMainCell = collectionVW.dequeueReusableCell(withIdentifier: "TestQuestionListCell", for: ) as! TestMainCell
    //        cell.optionheight.constant = 100
    //    }
    
    @IBOutlet weak var imgGredient: UIImageView!
    
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var collectionVW: UICollectionView!
    @IBOutlet weak var labelHeader: UILabel!
    
    @IBOutlet weak var ArrowPreviousBTN: UIButton!
    @IBOutlet weak var ArrowNextBTN: UIButton!
    
    @IBOutlet weak var PageCountLabel: UILabel!
    @IBOutlet weak var TestTimeLabel: UILabel!
    
    @IBOutlet weak var videoview: UIView!
    @IBOutlet weak var starttime: UILabel!
    @IBOutlet weak var endtime: UILabel!
    @IBOutlet weak var videoslider: UISlider!
    
    var finishedLoadingInitialCollectionCells = false
    var Question_Paper_List = [[String : Any]]()
    var Get_PaperID = ""
    var strSubject = String()
    
    var dataToQuestion: [QuestionList] = []
    var dataToOptions: [OptionsList] = []
    
    var Detail_array = [[String : Any]]()
    var arr_selected_Value = [[String : Any]]()
     var saveddata = [[String : Any]]()
    //MARK:- View Life Cycle
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        insertCSSString(into: webView) // 2
        
    }
    
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body {max-width:100%; width:auto !important; max-height:50%;height:50% !important; font-size: 15px; color: #fff } table { width:100% !important; color: #FFF; border-color: #FFF; border: white; border-collapse: collapse;} img{max-width:100%; width:auto !important; height:auto !important;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
    //    func insertContentsOfCSSFile(into webView: UIWebView) {
    //        guard let path = Bundle.main.path(forResource: "styles", ofType: "css") else { return }
    //        let cssString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines)
    //        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
    //        webView.stringByEvaluatingJavaScript(from: jsString)
    //    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaperID: \(Get_PaperID)")
        // Question_Paper_List_Api()
        
        self.PageCountLabel.text = "1/\(self.Question_Paper_List.count)"
        imgBG.setGredient()
        imgGredient.setGredient()
        labelHeader.text = strSubject
        ArrowPreviousBTN.isEnabled = false
        ArrowPreviousBTN.alpha = 0.5
        self.setsimplevideo()
    }
    
    //MARK:- WEB SERVICES
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var pageIndex = Int(scrollView.contentOffset.x/view.frame.width)
        pageIndex = pageIndex+1
        
        print("pageIndex => ",pageIndex)
        print("self.Question_Paper_List.count => ",self.Question_Paper_List.count)
        
        var String1 = String(pageIndex)
        String1 += "/"
        String1 += String(self.Question_Paper_List.count)
        self.PageCountLabel.text = String1
        
        //self.pageControl.currentPage = Int(pageIndex)
        
    }
    
    
    //MARK:- View Life Cycle
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEndTestAction(_ sender: Any) {
        
        //   Student_Submit_Test_Api()
        
        //        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
        //        secondVc.PaperID = Get_PaperID
        //        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    //MARK:- Collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Question_Paper_List.count
    }
    var cell = TestMainCell()
    var index = 0
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestMainCell", for: indexPath) as! TestMainCell
        cell.layoutIfNeeded()
        cell.currentIndex = indexPath.row
        cell.cellDelegate = self
        index = indexPath.row
        //        cell.frame = tableView.bounds;
        //        [cell layoutIfNeeded];
        //        [cell.collectionView reloadData];
        //        cell.collectionViewHeight.constant = cell.collectionView.collectionViewLayout.collectionViewContentSize.height;
        
        let second = (Question_Paper_List[indexPath.row]["QuestionMarkers"] as! Double)  - ((self.saveddata[0]["StartTime"] as? Double)!)
        cell.lbltime.text = self.stringFromTimeInterval(interval:second / 1000)
        print("MARKERRIME" ,second / 1000)
        let optiondata = Question_Paper_List[indexPath.row]["Question"] as? String ?? ""
        
        
        cell.webkit.loadHTMLString(optiondata, baseURL: nil)
        
        // cell.constWebviewHeight.constant = cell.webkit.collectionViewLayout.collectionViewContentSize.height
        cell.webkit.delegate = self
        cell.webkit.isOpaque = false
        cell.webkit.backgroundColor = UIColor.clear
        
        //         let value = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
        //         cell.dataToOptions.removeAll()
        
        cell.arr_Options.removeAll()
        cell.arr_Options = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
        
        //          for i in 0..<value.count {
        //            cell.arr_Options.append(value[i])
        //              let options = value[i]["Options"] as? String ?? ""
        //            cell.dataToOptions.append(OptionsList(Options: options.htmlToString))
        //          }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.tblList.reloadData()
        
        cell.layoutIfNeeded()
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 5 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimationForCollection()
        }
        //
        if indexPath.row == 0
        {
            ArrowPreviousBTN.isEnabled = false
            ArrowPreviousBTN.alpha = 0.5
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
        else if indexPath.row + 1 == Question_Paper_List.count
        {
            ArrowNextBTN.isEnabled = false
            ArrowNextBTN.alpha = 0.5
        }
        else
        {
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
        //
    }
    
    
    func didPressButton(arr: [String : Any] , index : Int) {
        let Ids = arr_selected_Value.map({ $0["AnswerID"] as? String ?? ""})
        if Ids.contains(arr["AnswerID"] as? String ?? "")
        {
            arr_selected_Value.remove(at: index)
        }
        arr_selected_Value.append(arr)
        print(arr_selected_Value)
    }
    
    @IBAction func arrowpreviusbtn_click(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionVW.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        
        if nextItem.row < Question_Paper_List.count && nextItem.row >= 0
        {
            self.collectionVW.scrollToItem(at: nextItem, at: .right, animated: true)
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
            if nextItem.row == 0
            {
                ArrowPreviousBTN.isEnabled = false
                ArrowPreviousBTN.alpha = 0.5
            }
        }
        else
        {
            ArrowPreviousBTN.isEnabled = false
            ArrowPreviousBTN.alpha = 0.5
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
    }
    
    @IBAction func arrownextbtn_click(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionVW.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        
        if nextItem.row < Question_Paper_List.count
        {
            self.collectionVW.scrollToItem(at: nextItem, at: .left, animated: true)
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
            if nextItem.row+1 == Question_Paper_List.count
            {
                ArrowNextBTN.isEnabled = false
                ArrowNextBTN.alpha = 0.5
            }
        }
        else
        {
            ArrowNextBTN.isEnabled = false
            ArrowNextBTN.alpha = 0.5
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
        }
    }
    
/* ============================================ MARK:- VIDEO VIEW CONFIGURATION ======================================================= */
    
    var timer = Timer()
    var player:AVPlayer?
    var playerItem:AVPlayerItem?
    var playButton:UIButton?
    var imgv:UIImageView?
    var timeObserver: Any?
    var seconds :Float64 = 0
    var markerarr:NSMutableArray = []
    
    func setsimplevideo()
    {
        let type = saveddata[0]["Type"] as! String
        if type == "1"
        {
            let url = URL(string: saveddata[0]["VideoURL"] as! String)
            let playerItem:AVPlayerItem = AVPlayerItem(url: url!)
            player = AVPlayer(playerItem: playerItem)
            
            let playerLayer=AVPlayerLayer(player: player!)
            playerLayer.frame=CGRect(x:0, y:0, width:self.videoview.frame.size.width, height:self.videoview.frame.size.height-100)
            imgv = UIImageView(frame: playerLayer.frame)
            if let url  = NSURL(string: saveddata[0]["VideoImage"] as! String),
                let data = NSData(contentsOf: url as URL)
            {
                imgv?.image = UIImage(data: data as Data)
            }
            self.videoview.addSubview(imgv!)
            self.videoview.layer.addSublayer(playerLayer)
            
            
            self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float((self.saveddata[0]["StartTime"] as? Double)!)/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            
            playButton = UIButton(type: UIButton.ButtonType.system) as UIButton
            let xPostion:CGFloat = 10
            let yPostion:CGFloat = self.videoview.frame.size.height-80
            let buttonWidth:CGFloat = 40
            let buttonHeight:CGFloat = 40
            
            playButton!.frame = CGRect(x:xPostion, y:yPostion, width:buttonWidth, height:buttonHeight)
            playButton?.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            playButton!.setImage(UIImage(named: "play"), for: .normal)
            playButton!.tintColor = UIColor.black
            playButton!.addTarget(self, action: #selector(TestQuestionVideoListView.playButtonTapped(_:)), for: .touchUpInside)
            
            self.videoview.addSubview(playButton!)
            for post in self.Question_Paper_List
            {
                let QuestionMarkers = post["QuestionMarkers"] as? Double
                self.markerarr.add(QuestionMarkers!)
            }
            for i in 0 ..< markerarr.count
            {
                let  slider = UISlider(frame:CGRect(x:videoslider.frame.origin.x, y:self.videoslider.frame.origin.y+15, width:self.videoslider.frame.size.width, height:30))
                
                slider.minimumValue = 0
                slider.maximumValue = Float((self.saveddata[0]["EndTime"] as? Double)!) - Float((self.saveddata[0]["StartTime"] as? Double)!)
                slider.value = Float(markerarr.object(at: i) as! Double) - Float((self.saveddata[0]["StartTime"] as? Double)!)
                slider.maximumTrackTintColor = UIColor.clear
                slider.minimumTrackTintColor = UIColor.clear
                slider.backgroundColor = UIColor.clear
                slider.setThumbImage(UIImage(named: "marker"), for: .normal)
              //  slider.thumbTintColor = #colorLiteral(red: 0, green: 0.646297574, blue: 0.6038294435, alpha: 1)
                self.videoview.addSubview(slider)
            }
            
            let duration : CMTime = playerItem.asset.duration
            seconds  = ((self.saveddata[0]["EndTime"] as? Double)! - (self.saveddata[0]["StartTime"] as? Double)!)/1000      //CMTimeGetSeconds(duration)
            self.endtime?.text = self.stringFromTimeInterval(interval: seconds)
            videoslider.maximumValue =   Float(seconds)
            videoslider.minimumValue =  0
            
            videoview.bringSubviewToFront(starttime)
            videoview.bringSubviewToFront(endtime)
            videoview.bringSubviewToFront(imgv!)
            
            videoslider.setThumbImage(UIImage(named: "thumb"), for: .normal)
            
            timeObserver = player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: nil) { (CmTime) -> Void in
                if self.player!.currentItem?.status == .readyToPlay {
                    let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                    self.videoslider.value = Float ( time - Float64((self.saveddata[0]["StartTime"] as? Double ?? 0))/1000 );
                    self.starttime?.text = self.stringFromTimeInterval(interval: (time - Float64((self.saveddata[0]["StartTime"] as? Double ?? 0))/1000))
                    let timestr = String(format: "%.0f", (time))
                    print("TIMERUNNING:",timestr)
                    let endtime = String(format: "%.0f", ((((self.saveddata[0]["EndTime"] as? Double)!))/1000))
                    if timestr == endtime
                    {
                        self.player?.seek(to: CMTimeMakeWithSeconds((Float64(Float(((self.saveddata[0]["StartTime"] as? Double)!) )/1000)), preferredTimescale: 1), toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
                        self.playButtonTapped(self.playButton!)
                        self.imgv?.isHidden = false
                        self.videoview.bringSubviewToFront(self.imgv!)
                       // self.finishVideo()
                    }
                }
            }
        }
        else
        {
        }
    }
    
    @objc func playButtonTapped(_ sender:UIButton)
    {
        self.imgv?.isHidden = true
        if self.player?.rate == 0
        {
            self.player!.play()
            self.playButton!.setImage(UIImage(named: "pause"), for: .normal)
            
        } else {
            self.player!.pause()
            self.playButton!.setImage(UIImage(named: "play"), for: .normal)
        }
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        
        if (hours > 0) {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }

}
