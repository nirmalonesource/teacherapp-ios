//
//  TestQuestionListView.swift
//  TeacherApp
//
//  Created by My Mac on 05/03/19.
//  Copyright © 2019 My Mac. All rights reserved.
//


import UIKit
import Alamofire
import SVProgressHUD
import SDWebImage

extension Int {
    func correspondingLetter(inUppercase uppercase: Bool = false) -> String? {
        let firstLetter = uppercase ? "A" : "a"
        let startingValue = Int(UnicodeScalar(firstLetter)!.value)
        if let scalar = UnicodeScalar(self + startingValue) {
            return String(scalar)
        }
        return nil
    }
}

extension UIWebView {
    ///Method to fit content of webview inside webview according to different screen size
    func resizeWebContent() {
        let contentSize = self.scrollView.contentSize
        let viewSize = self.bounds.size
        let zoomScale = viewSize.width/contentSize.width
        self.scrollView.minimumZoomScale = zoomScale
        self.scrollView.maximumZoomScale = zoomScale
        self.scrollView.zoomScale = zoomScale
    }
}

protocol SubCategoryProductDelegate : class {
    func didPressButton(arr : [String:Any] , index : Int)
}

struct QuestionList {
    var Question: String?
    init(Question: String) {
        self.Question = Question
    }
}

struct OptionsList {
    var Options: String?
    var IsCorrect: String?
    var MCQOptionID: String?
    var MCQQuestionID: String?
    
    init(Options: String) {
        self.Options = Options
    }
    init(IsCorrect: String) {
        self.IsCorrect = IsCorrect
    }
    init(MCQOptionID: String) {
        self.MCQOptionID = MCQOptionID
    }
    init(MCQQuestionID: String) {
        self.MCQQuestionID = MCQQuestionID
    }
}

class TestMainCell: UICollectionViewCell , UITableViewDelegate , UITableViewDataSource, UIWebViewDelegate
{
    @IBOutlet var lbltime: UILabel!
    @IBOutlet weak var webkit: UIWebView!
    @IBOutlet weak var lblTile: UILabel!
    @IBOutlet weak var tblList: UITableView!
    var finishedLoadingInitialTableCells = false
    var dataToOptions: [OptionsList] = []
    var arr_selected = [[String : String]]()
    var arr_Options = [[String : Any]]()
    var currentIndex = Int()
    @IBOutlet weak var constWebviewHeight: NSLayoutConstraint!
    weak var cellDelegate: SubCategoryProductDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tblList.delegate = self
        tblList.dataSource = self
        webkit.delegate = self
//        self.tblList.estimatedRowHeight = 150
//        self.tblList.rowHeight = UITableView.automaticDimension
    }
    
    /*
     - (void)webViewDidFinishLoad:(UIWebView *)webView
     {
     [tableView beginUpdates];
     [tableView endUpdates];
     }
     */
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
        constWebviewHeight.constant = webkit.scrollView.contentSize.height
        
//        let indexPath = IndexPath(row: self.index, section: 0)
//        let cell = self.tblList.cellForRow(at: indexPath) as! TestQuestionListCell
//
//        cell.optionheight.constant = cell.webKit.scrollView.contentSize.height
////        cell.optionheight.constant = cell.webKit.frame.size.height
//        cell.optionwebheight.constant = cell.webKit.frame.size.height
        self.layoutIfNeeded()
    }
    
    //MARK:- Webview Delegate
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body { width:100% !important; font-size: 15px; color: #000 } table { width:100% !important; color: #000; border-color: #000; border: black; border-collapse: collapse;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
//        setNeedsLayout()
        layoutIfNeeded()
        //   webkit.scrollView.isScrollEnabled = false
        var frame = webkit.frame
        
        frame.size.width = self.webkit.frame.size.width
        frame.size.height = 1
        
        webkit.frame = frame
        frame.size.height = webkit.scrollView.contentSize.height
        
        webkit.frame = frame;
        constWebviewHeight.constant = webkit.scrollView.contentSize.height + 50
        print("TOPWEBHEIGHT:",frame)
       //  self.layoutIfNeeded()
        tblList.frame.origin.y = webkit.scrollView.contentSize.height + 10
        print("TBLFRAME:",tblList.frame)
        webView.resizeWebContent()

//            webView.frame.size.height = 1;
//            webView.frame.size = webView.sizeThatFits(.zero)
        

        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
         print("CELLHEIGGHT:",webView.scrollView.contentSize.height as Any)
        if  webView.scrollView.contentSize.height > 56
        {
              contentHeights[webView.tag] = webView.scrollView.contentSize.height + 20
        }
        else
        {
            contentHeights[webView.tag] = webView.frame.size.height + 20
        }
      
        tblList.reloadRows(at: [IndexPath(row: webView.tag, section: 0)], with: .automatic)
               layoutIfNeeded()

    }
    
        var contentHeights : [CGFloat] = [0.0,0.0,0.0,0.0,0.0]
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr_Options.count
    }
    var cellindex:NSMutableArray = []
   // var cell = TestQuestionListCell()
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:TestQuestionListCell = tableView.dequeueReusableCell(withIdentifier: "TestQuestionListCell") as! TestQuestionListCell

        cell.layoutIfNeeded()
        
        let htmlHeight = contentHeights[indexPath.row]
        cell.lblRounderOption.setRadius(radius: cell.lblRounderOption.frame.height/2)
        cell.vwBG.setRadius(radius: 10)
        cell.vwBG.setShadow()
        cell.vwBG.layer.borderWidth = 0.5
        cell.vwBG.layer.borderColor = UIColor.lightGray.cgColor
        //  cell.vwBG.setTopShadow()
        cell.webKit.delegate = self
      //  cell.lblRounderOption.text = "\(indexPath.row + 1)"
        cell.lblRounderOption.text = indexPath.row.correspondingLetter(inUppercase: true)
        
        //        cell.webKit.loadHTMLString((dataToOptions[indexPath.row].Options ?? ""), baseURL: nil)
   //  cellindex.insert(1, at: indexPath.row)
        
        cell.webKit.tag = indexPath.row
        let optiondata = arr_Options[indexPath.row]["Options"] as? String ?? ""

        cell.webKit.loadHTMLString(optiondata , baseURL: nil)
       // cell.webKit.scrollView.isScrollEnabled = false
        cell.webKit.isOpaque = false
        cell.webKit.backgroundColor = UIColor.clear
        
        let arr : [String] = arr_selected.map({ $0["AnswerID"] ?? ""})
        if arr.contains(arr_Options[indexPath.row]["MCQOptionID"] as? String ?? "")
        {
            cell.vwBG.backgroundColor = UIColor.init(rgb: ConstantVariables.Constants.lightGreen)
        }
        else
        {
            cell.vwBG.backgroundColor = UIColor.white
        }
      
        cell.webKit.frame = CGRect(x: cell.webKit.frame.origin.x, y: 0, width: cell.webKit.frame.size.width, height: htmlHeight)
//        cell.webKit.frame.size.height = 1
//        cell.webKit.frame.size = (cell.webKit.sizeThatFits(CGSize.zero))
        
        cell.optionheight.constant = cell.webKit.scrollView.contentSize.height-10
     //   cell.contentView.frame.size.height = cell.webKit.scrollView.contentSize.height-10
        cell.optionwebheight.constant = cell.webKit.scrollView.contentSize.height-10
        
//       cell.setNeedsLayout()
        cell.layoutIfNeeded()
        //cell.updateConstraints()
       // cell.updateConstraintsIfNeeded()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     //    return contentHeights[indexPath.row]
        if contentHeights[indexPath.row] != 0.0 {
           //  let cell:TestQuestionListCell = tableView.dequeueReusableCell(withIdentifier: "TestQuestionListCell") as! TestQuestionListCell
            print("MYHEIGHT:",contentHeights[indexPath.row])
      
            return contentHeights[indexPath.row]
        }
        return 0
    }
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 100
//    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        
//        var dict = [String : String]()
//        //        dict["IsCorrect"] = arr_Options[indexPath.row]["IsCorrect"] as? String ?? ""
//        //        dict["MCQOptionID"] = arr_Options[indexPath.row]["MCQOptionID"] as? String ?? ""
//        //        dict["MCQQuestionID"] = arr_Options[indexPath.row]["MCQQuestionID"] as? String ?? ""
//        
//        //        [{"AnswerID":0,"IsAttempt":"0","MCQPlannerDetailsID":3}]
//        
//        dict["AnswerID"] = arr_Options[indexPath.row]["MCQOptionID"] as? String ?? ""
//        dict["IsAttempt"] = "1"
//        dict["MCQPlannerDetailsID"] = arr_Options[indexPath.row]["MCQQuestionID"] as? String ?? ""
//        
//        arr_selected.removeAll()
//        arr_selected.append(dict)
//        
//        tblList.reloadData()
//        cellDelegate?.didPressButton(arr: dict, index: currentIndex)
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 4 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimationForCollection()
        }
    }
    
}

class TestQuestionListCell: UITableViewCell,UIWebViewDelegate {
    
    @IBOutlet weak var webKit: UIWebView!
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblRounderOption: UILabel!
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var optionheight: NSLayoutConstraint!
    
    @IBOutlet weak var optionwebheight: NSLayoutConstraint!
   
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class TestQuestionListView: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , SubCategoryProductDelegate , UIWebViewDelegate
{
    
    @IBOutlet weak var BGview: UIView!
    //    override func viewDidLayoutSubviews() {
//        let cell:TestMainCell = collectionVW.dequeueReusableCell(withIdentifier: "TestQuestionListCell", for: ) as! TestMainCell
//        cell.optionheight.constant = 100
//    }
    @IBOutlet weak var imgGredient: UIImageView!
 
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var collectionVW: UICollectionView!
    @IBOutlet weak var labelHeader: UILabel!
    
    @IBOutlet weak var ArrowPreviousBTN: UIButton!
    @IBOutlet weak var ArrowNextBTN: UIButton!
    
    @IBOutlet weak var PageCountLabel: UILabel!
    @IBOutlet weak var TestTimeLabel: UILabel!
    
    var finishedLoadingInitialCollectionCells = false
    var Question_Paper_List = [[String : Any]]()
    var Get_PaperID = ""
    var strSubject = String()
    
    var dataToQuestion: [QuestionList] = []
    var dataToOptions: [OptionsList] = []
    
    var Detail_array = [[String : Any]]()
    var arr_selected_Value = [[String : Any]]()
    
    //MARK:- View Life Cycle
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        insertCSSString(into: webView) // 2

    }
    
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body {max-width:100%; width:auto !important; max-height:50%;height:50% !important; font-size: 15px; color: #fff } table { width:100% !important; color: #FFF; border-color: #FFF; border: white; border-collapse: collapse;} img{max-width:100%; width:auto !important; height:auto !important;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
//    func insertContentsOfCSSFile(into webView: UIWebView) {
//        guard let path = Bundle.main.path(forResource: "styles", ofType: "css") else { return }
//        let cssString = try! String(contentsOfFile: path).trimmingCharacters(in: .whitespacesAndNewlines)
//        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
//        webView.stringByEvaluatingJavaScript(from: jsString)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaperID: \(Get_PaperID)")
       // Question_Paper_List_Api()
        
           self.PageCountLabel.text = "1/\(self.Question_Paper_List.count)"
        imgBG.setGredient()
        imgGredient.setGredient()
        labelHeader.text = strSubject
        ArrowPreviousBTN.isEnabled = false
        ArrowPreviousBTN.alpha = 0.5

    }
    
    //MARK:- WEB SERVICES
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var pageIndex = Int(scrollView.contentOffset.x/view.frame.width)
        pageIndex = pageIndex+1
        
        print("pageIndex => ",pageIndex)
        print("self.Question_Paper_List.count => ",self.Question_Paper_List.count)
        
        var String1 = String(pageIndex)
        String1 += "/"
        String1 += String(self.Question_Paper_List.count)
        self.PageCountLabel.text = String1
        
        //self.pageControl.currentPage = Int(pageIndex)
        
    }
    

    //MARK:- View Life Cycle
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnEndTestAction(_ sender: Any) {
        
     //   Student_Submit_Test_Api()
        
        //        let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestReportView") as! TestReportView
        //        secondVc.PaperID = Get_PaperID
        //        self.navigationController?.pushViewController(secondVc, animated: true)
    }
    
    //MARK:- Collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Question_Paper_List.count
    }
    var cell = TestMainCell()
    var index = 0
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TestMainCell", for: indexPath) as! TestMainCell
        cell.layoutIfNeeded()
        cell.currentIndex = indexPath.row
        cell.cellDelegate = self
        index = indexPath.row
        //        cell.frame = tableView.bounds;
        //        [cell layoutIfNeeded];
        //        [cell.collectionView reloadData];
        //        cell.collectionViewHeight.constant = cell.collectionView.collectionViewLayout.collectionViewContentSize.height;
        
       // let dic = Question_Paper_List[indexPath.row]["QuestionMarkers"] as? Double
       // cell.lbltime.text = String(Question_Paper_List[indexPath.row]["QuestionMarkers"] as! Double / 1000)
       
        let optiondata = Question_Paper_List[indexPath.row]["Question"] as? String ?? ""

        
        cell.webkit.loadHTMLString(optiondata, baseURL: nil)
        
        // cell.constWebviewHeight.constant = cell.webkit.collectionViewLayout.collectionViewContentSize.height
        cell.webkit.delegate = self
        cell.webkit.isOpaque = false
        cell.webkit.backgroundColor = UIColor.clear
        
        //         let value = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
        //         cell.dataToOptions.removeAll()
        
        cell.arr_Options.removeAll()
        cell.arr_Options = Question_Paper_List[indexPath.row]["Options"] as? [[String : Any]]  ?? []
        
        //          for i in 0..<value.count {
        //            cell.arr_Options.append(value[i])
        //              let options = value[i]["Options"] as? String ?? ""
        //            cell.dataToOptions.append(OptionsList(Options: options.htmlToString))
        //          }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        cell.tblList.reloadData()
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 5 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimationForCollection()
        }
        //
        if indexPath.row == 0
        {
            ArrowPreviousBTN.isEnabled = false
            ArrowPreviousBTN.alpha = 0.5
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
        else if indexPath.row + 1 == Question_Paper_List.count
        {
            ArrowNextBTN.isEnabled = false
            ArrowNextBTN.alpha = 0.5
        }
        else
        {
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
        //
    }
    
    
    func didPressButton(arr: [String : Any] , index : Int) {
        let Ids = arr_selected_Value.map({ $0["AnswerID"] as? String ?? ""})
        if Ids.contains(arr["AnswerID"] as? String ?? "")
        {
            arr_selected_Value.remove(at: index)
        }
        arr_selected_Value.append(arr)
        print(arr_selected_Value)
    }
    
    @IBAction func arrowpreviusbtn_click(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionVW.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item - 1, section: 0)
        
        if nextItem.row < Question_Paper_List.count && nextItem.row >= 0
        {
            self.collectionVW.scrollToItem(at: nextItem, at: .right, animated: true)
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
            if nextItem.row == 0
            {
                ArrowPreviousBTN.isEnabled = false
                ArrowPreviousBTN.alpha = 0.5
            }
        }
        else
        {
            ArrowPreviousBTN.isEnabled = false
            ArrowPreviousBTN.alpha = 0.5
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
        }
    }
    
    @IBAction func arrownextbtn_click(_ sender: UIButton) {
        
        let visibleItems: NSArray = self.collectionVW.indexPathsForVisibleItems as NSArray
        let currentItem: IndexPath = visibleItems.object(at: 0) as! IndexPath
        let nextItem: IndexPath = IndexPath(item: currentItem.item + 1, section: 0)
        
        if nextItem.row < Question_Paper_List.count
        {
            self.collectionVW.scrollToItem(at: nextItem, at: .left, animated: true)
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
            ArrowNextBTN.isEnabled = true
            ArrowNextBTN.alpha = 1.0
            if nextItem.row+1 == Question_Paper_List.count
            {
                ArrowNextBTN.isEnabled = false
                ArrowNextBTN.alpha = 0.5
            }
        }
        else
        {
            ArrowNextBTN.isEnabled = false
            ArrowNextBTN.alpha = 0.5
            ArrowPreviousBTN.isEnabled = true
            ArrowPreviousBTN.alpha = 1.0
        }
    }
    
}
extension Collection where Iterator.Element == [Int:[String]] {
    
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[Int:[String]]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}
