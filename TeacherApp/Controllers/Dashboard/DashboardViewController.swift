//
//  DashboardViewController.swift
//  TeacherApp
//
//  Created by archana on 25/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class DashboardListCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblPaperTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class DashboardViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , CalendarViewDataSource, CalendarViewDelegate{

    @IBOutlet weak var vwCal: CalendarView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    var finishedLoadingInitialTableCells = false
    var selectedDate = String()
    var selectedMonth = String()
    
    var paperList = [[String : Any]]()
    var paperDateList = [[String : Any]]()
    var isfirst = true
    
    //MARK:- Web service
    
    func getParerList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["Date" : selectedDate ,
                              "Month" : selectedMonth
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PAPER_LIST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got Paper list Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.paperList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.paperList = resData["data"]!["paper_info"] as? [[String:Any]] ?? []
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        self.tblVW.reloadData()
                        self.view.layoutIfNeeded()
                        self.constTblHeight.constant = self.tblVW.contentSize.height
                        
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func getParerDatesList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = [
                              "Month" : selectedMonth
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PAPER_DATE_LIST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got paper date list Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.paperDateList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.paperDateList = resData["data"] as? [[String:Any]] ?? []
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        self.setAllDates()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func setAllDates()
    {
        var allDates = paperDateList.map({ $0["date"] as? String ?? ""})
        var strDate = String()
        var event = [Any]()
        
        for i in 0..<allDates.count
        {
            strDate = allDates[i]
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            let date: Date? = dateFormatterGet.date(from: strDate)!
            let a = dateFormatterGet.string(from: date!)
            
            let select_date: Date? = dateFormatterGet.date(from: a)!
            event.append(CalendarEvent(title: "red", startDate:  select_date!, endDate:  select_date!) )
            CalendarView.Style.cellEventColor = UIColor.red
            
        }
        
        self.vwCal.loadEvents(events: event as! [CalendarEvent]) { error in
            if error != nil {
            }
        }
        vwCal.reloadData()
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBG.setGredient()
        tblVW.tableFooterView = UIView()
        
        self.navigationController?.isNavigationBarHidden = true
        
        CalendarView.Style.cellColorDefault         = UIColor.white
        CalendarView.Style.cellColorToday           = UIColor.red
        CalendarView.Style.cellSelectedBorderColor  = UIColor.blue
        //        CalendarView.Style.cellEventColor           = UIColor.magenta
        CalendarView.Style.headerTextColor          = UIColor.black
        CalendarView.Style.cellTextColorDefault     = UIColor.black
        CalendarView.Style.cellTextColorToday       = UIColor.blue
        CalendarView.Style.cellShape = .round
        CalendarView.Style.firstWeekday             = .sunday
        
        vwCal.dataSource = self
        vwCal.delegate = self
        
        vwCal.direction = .horizontal
        vwCal.multipleSelectionEnable = true
        vwCal.marksWeekends = true
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let resultTime11 = formatter.string(from: Date())
        print(resultTime11)
        selectedDate = resultTime11
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM/yyyy"
        let resultTime2 = formatter2.string(from: Date())
        selectedMonth = resultTime2
        getParerList()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addTab(strView: "Dashboard")
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblVW.contentSize.height
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        let today = Date()
        
        var tomorrowComponents = DateComponents()
        tomorrowComponents.day = 1
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone.current
                let tomorrow2 = Calendar.current.date(byAdding: .day, value: 0, to: date)
                let tomorrow3 = Calendar.current.date(byAdding: .day, value: 1, to: date)
        
                let event = [CalendarEvent(title: "title111", startDate:  tomorrow2!, endDate:  tomorrow2!) ,
                             CalendarEvent(title: "title111", startDate:  tomorrow3!, endDate:  tomorrow2!)]
        
//                self.vwCal.loadEvents(events: event) { error in
//                    if error != nil {
//                    }
//                }
        vwCal.reloadData()
        
        self.vwCal.setDisplayDate(today)
        
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var myCustomView: DateHeaderView?
        myCustomView = Bundle.main.loadNibNamed("DateHeaderView", owner: self, options: nil)?.first as? DateHeaderView
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy/MM/dd"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE , MMM dd yyyy"
        
        let date: Date? = dateFormatterGet.date(from: "\(selectedDate)")!
        print(dateFormatter.string(from: date!))
        
        myCustomView?.lblDate.text = dateFormatter.string(from: date!)
        myCustomView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 40)
        
        return myCustomView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paperList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:DashboardListCell = tableView.dequeueReusableCell(withIdentifier: "DashboardListCell") as! DashboardListCell
        
        cell.lblPaperTitle.text = paperList[indexPath.row]["PlannerName"] as? String
        
       // cell.lblTime.text = paperList[indexPath.row]["EndDate"] as? String
        
        let df = paperList[indexPath.row]["EndDate"] as? String ?? ""
        let getDate = setDateWithFormat(strDate: df)
        cell.lblTime.text = getDate
    
        cell.lblSubTitle.text = "\(paperList[indexPath.row]["SubjectName"] as? String ?? "") | " + "\(paperList[indexPath.row]["StandardName"] as? String ?? "") | " + "\(paperList[indexPath.row]["MediumName"] as? String ?? "")" + "(\(paperList[indexPath.row]["BoardName"] as? String ?? ""))"

        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        
        if paperList.count > 0 && !finishedLoadingInitialTableCells {
            
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
                
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyPlanViewController") as! MyPlanViewController
        nextViewController.dictData = paperList[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd ,yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
    
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        
         let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
         self.addChild(popOverVC)
         let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
         popOverVC.view.frame = sd
            //self.view.frame
         self.view.addSubview(popOverVC.view)
         popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
        
    }
    
    // MARK : Events
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        vwCal.goToPreviousMonth()
    }
    
    @IBAction func goToNextMonth(_ sender: Any) {
        vwCal.goToNextMonth()
    }
}

extension DashboardViewController {
    
    // MARK : KDCalendarDataSource
    
    func startDate() -> Date {
        
        var dateComponents = DateComponents()
        //        dateComponents.month = -3
        dateComponents.year = -1;
        
        let today = Date()
        
        let threeMonthsAgo = vwCal.calendar.date(byAdding: dateComponents, to: today)!
        
        return threeMonthsAgo
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 2;
        let today = Date()
        
        let twoYearsFromNow = vwCal.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
    
    // MARK : KDCalendarDelegate
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        
        print("Did Select: \(date) with \(events.count) events")
        for event in events {
            print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let resultTime11 = formatter.string(from:  date)
//        let resultTime = formatter.string(from: Date())
        
        print(resultTime11)
        
        selectedDate = resultTime11
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "MM/yyyy"
        let resultTime2 = formatter2.string(from: date)
        selectedMonth = resultTime2

       getParerList()
        
        //        if Int(resultTime11.replacingOccurrences(of: "-", with: ""))! < Int(resultTime.replacingOccurrences(of: "-", with: ""))!
        //        {
        //            showViewWithAnimation(vw: vwSinglePopup, img: imgBlur)
        //        }
        //        else
        //        {
        //            showViewWithAnimation(vw: vwPopup, img: imgBlur)
        //        }
        
        
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        
        if isfirst
        {
            isfirst = false
            let resultTime11 = formatter.string(from: Date())
            print(resultTime11)
            selectedDate = resultTime11
            
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM/yyyy"
            let resultTime2 = formatter2.string(from: Date())
            selectedMonth = resultTime2
        }
        else
        {
            let resultTime11 = formatter.string(from: date)
            print(resultTime11)
            selectedDate = resultTime11
            
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "MM/yyyy"
            let resultTime2 = formatter2.string(from: date)
            selectedMonth = resultTime2
        }
        getParerDatesList()
        
    }
    
}


extension DashboardViewController {
    
    func selectedDates(_ dates: [Date]) {
        print(dates)
        
        if dates.count > 0
        {
//            selectedDate = "\(dates[0])"
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let resultTime = formatter.string(from: date)
            let resultTime11 = formatter.string(from: dates[0])
            
//            selectedDate = resultTime11
            
            //            if Int(resultTime11.replacingOccurrences(of: "-", with: ""))! < Int(resultTime.replacingOccurrences(of: "-", with: ""))!
            //            {
            //                showViewWithAnimation(vw: vwSinglePopup, img: imgBlur)
            //            }
            //            else
            //            {
            //                showViewWithAnimation(vw: vwPopup, img: imgBlur)
            //            }
            
        }
    }
    
}
