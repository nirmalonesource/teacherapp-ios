//
//  QuestionView.swift
//  TeacherApp
//
//  Created by My Mac on 16/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class QuestionView: UIViewController , UITableViewDelegate , UITableViewDataSource ,UIWebViewDelegate{
    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var tblquestions: UITableView!
    @IBOutlet weak var tblheightconstant: NSLayoutConstraint!
    @IBOutlet weak var textview: UITextView!
    
    var Questions_list = [[String : Any]]()
    var finishedLoadingInitialTableCells = false
    var isfilterclicked = Bool()
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        for i in 0..<Questions_list.count
        {
            contentHeights.insert(0.0, at: i)
        }
        imgGredient.setGredient()
        tblquestions.rowHeight = UITableView.automaticDimension
        tblquestions.estimatedRowHeight = UITableView.automaticDimension
        tblquestions.setRadius(radius: 10)
       
//        let html = Questions_list[1]["Question"] as! String
//        textview.attributedText = html.htmlToAttributedString

        //textview.attri
       // tblquestions.register(detailedreportcell.self, forCellReuseIdentifier: "cell2")
       // tblquestions.tableFooterView = UIView()
//        tblquestions.roundCorners(corners: [.topLeft,.topRight], radius: 10)
//        tblquestions.layer.borderColor = UIColor.lightGray.cgColor
//        tblquestions.layer.borderWidth = 0.5
       // tblquestions.register(UINib(nibName: "Reportcell", bundle: nil), forCellReuseIdentifier: "cell")
        self.view.layoutIfNeeded()

        
    }
    override func viewDidLayoutSubviews() {
            self.tblheightconstant.constant = self.tblquestions.contentSize.height
    }
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body {max-width:100%; width:auto !important; max-height:50%;height:50% !important; font-size: 15px; color: #fff } table { width:100% !important; color: #FFF; border-color: #FFF; border: white; border-collapse: collapse;} img{max-width:100%; width:auto !important; height:auto !important;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    //    func insertCSSString(into webView: UIWebView) {
    //        let cssString = "body { width:100% !important; font-size: 15px; color: #000 } table { width:100% !important; color: #000; border-color: #000; border: black; border-collapse: collapse;}"
    //        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
    //        webView.stringByEvaluatingJavaScript(from: jsString)
    //    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
       //  insertCSSString(into: webView)
        //        setNeedsLayout()
        
        //   webkit.scrollView.isScrollEnabled = false
//        let indexPath = IndexPath(row: webView.tag, section: 0)
//        let cell = self.tblquestions.cellForRow(at: indexPath) as! detailedreportcell
//     //   let cell = tblquestions.dequeueReusableCell(withIdentifier: "cell") as! Reportcell
//
        var frame = webView.frame
        print("WEBTAG:",webView.tag)

        frame.size.width = webView.frame.size.width
        frame.size.height = 1

        webView.frame = frame
        frame.size.height = webView.scrollView.contentSize.height
//
//        cell.webview.frame = frame;
//        // constWebviewHeight.constant = webView.scrollView.contentSize.height + 50
//        print("TOPWEBHEIGHT:",frame)
//        //  self.layoutIfNeeded()
//        print("TBLFRAME:",tblquestions.frame)
       // webView.resizeWebContent()

        //            webView.frame.size.height = 1;
        //            webView.frame.size = webView.sizeThatFits(.zero)

        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        print("CELLHEIGGHT:",webView.scrollView.contentSize.height as Any)
        contentHeights[webView.tag] = webView.scrollView.contentSize.height

       self.tblquestions.reloadRows(at: [IndexPath(row: webView.tag, section: 0)], with: .none)
         //     layoutIfNeeded()
     //       self.tblheightconstant.constant = self.tblquestions.contentSize.height
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Questions_list.count
    }
    
    var contentHeights : [CGFloat] = [0.0,0.0,0.0,0.0,0.0]
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:detailedreportcell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! detailedreportcell
      //  let cell:detailedreportcell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! detailedreportcell
       
            cell.layoutIfNeeded()
        
            cell.lblcount.text = String(indexPath.row + 1) + ")"
        
           // cell.lblcorrect.text = String (Questions_list[indexPath.row]["Correct"] as! Int)
//        cell.lblcorrect.text = String (format: "%d%%", Questions_list[indexPath.row]["Correct"] as? Float ?? 0)
//        cell.lblincorrect.text  = String (format: "%d%%", Questions_list[indexPath.row]["InCorrect"] as? Float ?? 0)
//        cell.lblnotattempt.text = String (format: "%d%%", Questions_list[indexPath.row]["NotAttempt"] as? Float ?? 0)
        
        let s1:String = String(format:"%.2f%%",  (Questions_list[indexPath.row]["Correct"] as! NSNumber).doubleValue)
        let s2:String = String(format:"%.2f%%",  (Questions_list[indexPath.row]["InCorrect"] as! NSNumber).doubleValue)
        let s3:String = String(format:"%.2f%%",  (Questions_list[indexPath.row]["NotAttempt"] as! NSNumber).doubleValue)
        cell.lblcorrect.text = s1
        cell.lblincorrect.text = s2
        cell.lblnotattempt.text = s3
//            cell.lblincorrect.text = String(Questions_list[indexPath.row]["InCorrect"] as! Int)
//            cell.lblnotattempt.text = String(Questions_list[indexPath.row]["NotAttempt"] as! Int)
          //  cell.webview.delegate = self
        
//            let htmlHeight = contentHeights[indexPath.row]
//            if htmlHeight > 1
//            {
//            }
//            else
//            {
//
//
//            }
        let html = Questions_list[indexPath.row]["Question"] as? String
        cell.webview.loadHTMLString(html!, baseURL: nil)
       // cell.textview.attributedText = html!.htmlToAttributedString
         cell.textview.attributedText = html?.htmlAttributed(family: "Helvetica", size: 14, color: UIColor.init(red: 0, green: 0, blue: 0))
            cell.webview.tag = indexPath.row
          //  cell.webview.frame = CGRect(x: cell.webview.frame.origin.x, y: 0, width: cell.webview.frame.size.width, height: htmlHeight)
            // cell.webview.frame = CGRect(x: cell.webview.frame.origin.x, y: 0, width: cell.webview.frame.size.width, height: cell.webview.scrollView.contentSize.height)
            cell.webheightconstant.constant = cell.textview.contentSize.height
            tblheightconstant.constant = tblquestions.contentSize.height
            self.viewDidLayoutSubviews()
            cell.selectionStyle = .none
            cell.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
       //  print("CONTENTHEIGHT:",contentHeights[indexPath.row] + 74)
         //  return contentHeights[indexPath.row]
//        if contentHeights[indexPath.row] != 0 {
//            return contentHeights[indexPath.row]
//        }
        return UITableView.automaticDimension
     //   return 204
        //        if contentHeights[indexPath.row] != 0.0 {
        //            print("MYHEIGHT:",contentHeights[indexPath.row])
        //            tblheightconstant.constant = tblquestions.contentSize.height
        //            return contentHeights[indexPath.row]
        //        }
        //        return 0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    @IBAction func filter_click(_ sender: UIButton) {
        
        if isfilterclicked
        {
            isfilterclicked = false
            Questions_list.sort{
                ((($0 as Dictionary<String, AnyObject>)["Correct"] as! NSNumber).doubleValue) < ((($1 as Dictionary<String, AnyObject>)["Correct"] as! NSNumber).doubleValue)
            }
        }
        else
        {
             isfilterclicked = true
            Questions_list.sort{
               ((($0 as Dictionary<String, AnyObject>)["Correct"] as! NSNumber).doubleValue) > ((($1 as Dictionary<String, AnyObject>)["Correct"] as! NSNumber).doubleValue)
            }
        }
        
        tblquestions.reloadData()

    }
    
}
