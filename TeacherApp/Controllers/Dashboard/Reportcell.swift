//
//  Reportcell.swift
//  TeacherApp
//
//  Created by My Mac on 12/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class Reportcell: UITableViewCell {

    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var lblcorrect: UILabel!
    @IBOutlet weak var lblincorrect: UILabel!
    @IBOutlet weak var lblnotattempt: UILabel!
    @IBOutlet weak var webheightconstant: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
