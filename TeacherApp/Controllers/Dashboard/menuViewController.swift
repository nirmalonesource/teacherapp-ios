//
//  menuViewController.swift
//  TeacherApp
//
//  Created by My Mac on 30/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
class menuCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
}

class menuViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    
    @IBOutlet weak var heightConstrants: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnOutlet: UIButton!
    let DataArray = ["Dashboard","My Profile","Query? Call Up","Rate us","Legal","Logout"]
    
    @IBOutlet weak var popupView: UIView!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        popupView.roundCorners(corners: [.bottomRight], radius: popupView.frame.height/2)
        self.tblView.tableFooterView = UIView()
        let gestureRecognizerOne = UITapGestureRecognizer(target: self, action: #selector(tap))
        imgBlur.addGestureRecognizer(gestureRecognizerOne)
        self.view.layoutIfNeeded()
        
    }
    
    @objc func tap() {
        hideViewWithAnimation(vw: mainView, img: imgBlur)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.layoutIfNeeded()
        heightConstrants.constant = tblView.contentSize.height
        popupView.roundCorners(corners: [.bottomRight], radius: popupView.frame.height/2)
        self.view.layoutIfNeeded()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! menuCell
        cell.lblName.text =  DataArray[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else if indexPath.row == 1 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileView") as! MyProfileView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else if indexPath.row == 2 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QueryView") as! QueryView
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else if indexPath.row == 3
        {
            rateApp(appId: "id1451784807") { success in
                print("RateApp \(success)")
            }
        }
        else if indexPath.row == 4 {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LegalViewController") as! LegalViewController
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else{
            
            let alert = EMAlertController(title: ConstantVariables.Constants.Project_Name, message: "Are you sure you want to Logout?")
            
            let action1 = EMAlertAction(title: "Logout", style: .cancel)
            {
                let domain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: domain)
                UserDefaults.standard.synchronize()
                print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                
                let loginView: LoginView = self.storyboard!.instantiateViewController(withIdentifier: "LoginView") as! LoginView
                let navController = UINavigationController.init(rootViewController: loginView)
                UIApplication.shared.keyWindow?.rootViewController = navController
            }
            
            let action2 = EMAlertAction(title: "Cancel", style: .normal) {
                // Perform Action
            }
            
            alert.addAction(action: action1)
            alert.addAction(action: action2)
            
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func btnActionClicked(_ sender: UIButton) {
        mainView.isHidden = !mainView.isHidden
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
}
