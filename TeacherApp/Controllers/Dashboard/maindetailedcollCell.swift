//
//  maindetailedcollCell.swift
//  TeacherApp
//
//  Created by My Mac on 21/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class maindetailedcollCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var lblperformance: UIButton!
    @IBOutlet weak var lblavgcount: UILabel!
    @IBOutlet weak var innercollvw: UICollectionView!
    
    var finishedLoadingInitialCollectionCells = false
    var data = [String : Any]()
    
    override func awakeFromNib() {
       // innercollvw.register(ReportLecelCell.self, forCellWithReuseIdentifier: "ReportLecelCell"); //register custom UICollectionViewCell class.
        innercollvw.dataSource = self
        innercollvw.delegate = self
    }
      //  setupViews();
        // Here I am not using any custom class
        
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          print("PRINTTTT:%@",(data["level_accuracy"] as? [AnyObject] ?? []).count)
        return (data["level_accuracy"] as? [AnyObject] ?? []).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportLecelCell", for: indexPath) as! ReportLecelCell
        cell.layoutIfNeeded()
        cell.lblRadius.setRadius(radius: cell.lblRadius.frame.height/2)
        cell.lblRadius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        print("PRINTTTT:%@",((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String as Any)
        cell.lblLevel.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String
        //let TotalAvarage = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalAvarage") as? Float
        let TotalAvarage = (((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalAvarage") as! NSNumber).doubleValue
        if TotalAvarage == 0
        {
            cell.lblRadius.text = String(format: "%.0f %%", TotalAvarage )
        }
        else
        {
            let TotalCorrect = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalCorrect") as? Int
            let NoOfQuestions = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
            cell.lblRadius.text = String(format: "%.1f %% \n %d-%@", TotalAvarage ,TotalCorrect ?? 0,NoOfQuestions ?? "0")
        }
        //cell.lblRadius.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/3 - 10 , height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimation()
        }
    }
    
    
}
