//
//  MyPlanViewController.swift
//  TeacherApp
//
//  Created by My Mac on 30/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Foundation
 // import ActionSheetPicker_3_0
import Alamofire
import SVProgressHUD
import SwiftyPickerPopover

class CollectionCell: UICollectionViewCell {
    @IBOutlet weak var lblLevel: UILabel!
    @IBOutlet weak var lblRound: UILabel!
}

class tableViewCell: UITableViewCell {
    @IBOutlet weak var lblBatch: UILabel!
    @IBOutlet var btnbatch: UIButton!
    @IBOutlet var btnbatchimg: UIButton!
    
}

class ChapterListCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
}

 class MyPlanViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwPopup: UIView!
    @IBOutlet weak var lblviceEnglish: UILabel!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblChapter: UILabel!
    @IBOutlet weak var lblEndDate: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCollectionView: UICollectionView!
    
    @IBOutlet weak var constReportWidth: NSLayoutConstraint!
    @IBOutlet weak var constShareHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constShareWidth: NSLayoutConstraint!
    @IBOutlet weak var constReportHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constTblChapterHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var tblTableView: UITableView!
    
    @IBOutlet weak var tblChapter: UITableView!
    @IBOutlet weak var imgTrans: UIImageView!
    var dictData = [String : Any]()
    var chapterData = [[String : Any]]()
    var strSelectedDate = String()
     var selectedID = [String]()
    //MARK:- View Life Cycle 
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layoutIfNeeded()
        imgBlur.setGredient()
        vwPopup.setRadius(radius: 10)
        
        btnShare.setRadius(radius: btnShare.frame.height/2)
        btnReport.setRadius(radius: btnReport.frame.height/2)

        lblviceEnglish.text = "\(dictData["StandardName"] as? String ?? "")" + "(\(dictData["BoardName"] as? String ?? "")" + "-\(dictData["MediumName"] as? String ?? ""))"
        lblSubject.text = "\(dictData["SubjectName"] as? String ?? "")"
        lblStartDate.text = setDateFormat(strDate: dictData["StartDate"] as? String ?? "")
        lblEndDate.text = setDateFormat(strDate: dictData["EndDate"] as? String ?? "")
        lblTitle.text = "\(dictData["PlannerName"] as? String ?? "")"
        
        chapterData = dictData["chapter"] as? [[String : Any]] ?? []
        
        tblChapter.setRadius(radius: 10)
        tblChapter.tableFooterView = UIView()
        tblTableView.tableFooterView = UIView()
        
        if (dictData["avilable_batch"] as? Int ?? 0) <= 0
        {
//            constShareHeight.constant = 0
            btnShare.isHidden = true
        }
        
        /*
         @IBOutlet myConstraint : NSLayoutConstraint!
         @IBOutlet myView : UIView!
         
         func updateConstraints() {
         // You should handle UI updates on the main queue, whenever possible
         DispatchQueue.main.async {
         myConstraint.constant = 10
         myView.layoutIfNeeded()
         }
         imageView.frame.size.height;
         Superview.leadingMargin*6
         }
     */
        
        if (dictData["published_batch"] as? [AnyObject] ?? []).count <= 0
        {
//            constReportHeight.constant = 0
            btnReport.isHidden = true
        }
        
        if btnShare.isHidden == false {
           constShareWidth.constant = ScreenSize.SCREEN_WIDTH - (ScreenSize.SCREEN_WIDTH / 2)
        }
            
        else if btnReport.isHidden == false {
            constReportWidth.constant = ScreenSize.SCREEN_WIDTH - (ScreenSize.SCREEN_WIDTH / 2)
        }
        
        lblChapter.text = "\(chapterData.count) Chapter Selected..."
        tblChapter.reloadData()
        tblTableView.reloadData()
        
        self.view.layoutIfNeeded()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblTableView.contentSize.height
//        constTblChapterHeight.constant = tblChapter.contentSize.height
    }
    
    //MARK:- Collection method
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (dictData["level"] as? [AnyObject] ?? []).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "callCell", for: indexPath) as! CollectionCell
        cell.lblRound.setRadius(radius: cell.lblRound.frame.height/2)
        cell.lblRound.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
       
        cell.lblLevel.text = ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String
        cell.lblRound.text = ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let type = dictData["Type"] as? String ?? ""
        let dic = ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "level_question") as! [[String : AnyObject]]
        if dic.count > 0
        {
            if type == "1"
            {
                let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionVideoListView") as! TestQuestionVideoListView
                
                secondVc.strSubject =  ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String ?? ""
                secondVc.Question_Paper_List = dic
                secondVc.saveddata = [dictData]
                self.navigationController?.pushViewController(secondVc, animated: true)
            }
            else
            {
                let secondVc = self.storyboard?.instantiateViewController(withIdentifier: "TestQuestionListView") as! TestQuestionListView
                let dic = ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "level_question") as! [[String : AnyObject]]
                secondVc.strSubject =  ((dictData["level"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String ?? ""
                secondVc.Question_Paper_List = dic
                self.navigationController?.pushViewController(secondVc, animated: true)
            }
        }
        
    }
    
    //MARK:- Button action
    
    @IBAction func btnBackActionClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReportAction(_ sender: UIButton) {
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportView") as! ReportView
//        nextViewController.MCQPlannerID = dictData["MCQPlannerID"] as? String ?? "0"
//        self.navigationController?.pushViewController(nextViewController, animated: true)
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "NewReportView") as! NewReportView
        nextViewController.MCQPlannerID = dictData["MCQPlannerID"] as? String ?? "0"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnShareAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
        nextViewController.MCQPlannerID = dictData["MCQPlannerID"] as? String ?? "0"
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnMoreAction(_ sender: UIButton) {
        showViewWithAnimation(vw: tblChapter, img: imgTrans)
        btnClose.isHidden = false
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        hideViewWithAnimation(vw: tblChapter, img: imgTrans)
        btnClose.isHidden = true
    }
    
}

extension MyPlanViewController: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var myCustomView: DateHeaderView?
        myCustomView = Bundle.main.loadNibNamed("DateHeaderView", owner: self, options: nil)?.first as? DateHeaderView
      
        myCustomView?.lblDate.text = chapterData[section]["ChapterName"] as? String ?? ""
        myCustomView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 40)
        
        return myCustomView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == tblChapter ? 40 : 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView == tblChapter ? chapterData.count :  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == tblChapter ? (chapterData[section]["sub_chapter"] as? [Any] ?? []).count : (dictData["published_batch"] as? [AnyObject] ?? []).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblChapter
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChapterListCell", for: indexPath) as! ChapterListCell
            cell.lblTitle.text = ((chapterData[indexPath.section]["sub_chapter"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "ChapterName") as? String
            
//            cell.lblTitle.text = (((dictData["chapter"] as AnyObject).value(forKey: "sub_chapter") as AnyObject).objectAt(indexPath.row) as AnyObject).value(forKey: "ChapterName") as? String ?? ""
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! tableViewCell
            cell.lblBatch.text = ((dictData["published_batch"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "Batch") as? String
           // cell.btnbatch.setTitle(((dictData["published_batch"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "Batch") as? String, for: .normal)
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "yyyy-MM-dd"
            
            let date1: Date? = dateFormatter1.date(from: "\(((dictData["published_batch"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "PublishDate") as? String ?? "")")
            
            cell.btnbatch.setTitle(String(format: "%@ (%@)", ((self.dictData["published_batch"] as AnyObject).object(at:indexPath.row) as AnyObject).value(forKey: "Batch") as? String ?? "",(dateFormatter1.string(from: date1!))), for: .normal)
            cell.btnbatchimg.tag = indexPath.row
            cell.btnbatchimg.addTarget(self, action: #selector(btnbatchaction(sender:)), for: .touchUpInside)

            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    @objc func btnbatchaction(sender: UIButton){
        self.view.endEditing(true)
      
//        let datePicker = ActionSheetDatePicker(title: "Select Date:", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
//            picker, value, index in
//            picker?.minimumDate =  Date()
//            let dateFormatterGet = DateFormatter()
//            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MMM dd , yyyy"
//
//            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
//            print(dateFormatter.string(from: date!))
//           // sender.setTitle( "\(dateFormatter.string(from: date!))", for: .normal)
//
//            let dateFormatter1 = DateFormatter()
//            dateFormatter1.dateFormat = "yyyy-MM-dd"
//
//            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
//            self.strSelectedDate = (dateFormatter1.string(from: date1!))
//            sender.setTitle(String(format: "%@ (%@)", ((self.dictData["published_batch"] as AnyObject).object(at: sender.tag) as AnyObject).value(forKey: "Batch") as? String ?? "",(dateFormatter1.string(from: date1!))), for: .normal)
//            print("Selected : \(self.strSelectedDate)")
//             self.selectedID.append(((self.dictData["published_batch"] as AnyObject).object(at: sender.tag) as AnyObject).value(forKey: "BatchID") as? String ?? "0")
//            self.publishPaper()
//
//            return
//        }, cancel: { ActionStringCancelBlock in return },
//           origin: sender.superview!.superview)
//        datePicker?.show()
        
        
        DatePickerPopover(title: "Select Date")
               .setDateMode(.date)
               //.setMaximumDate(Date())
               .setSelectedDate(Date())
               .setDoneButton(action: {
                   popover, selectedDate in
                   
                   print("selectedDate \(selectedDate)")
                   let formatter = DateFormatter()
                       formatter.dateFormat = "dd-MM-yyyy"
                  sender.setTitle("\(formatter.string(from: selectedDate))", for: .normal)
                 
                  let dateFormatter1 = DateFormatter()
                     dateFormatter1.dateFormat = "yyyy-MM-dd"
                 self.strSelectedDate = (dateFormatter1.string(from: selectedDate))
                     print("Selected : \(self.strSelectedDate)")
                
                
                sender.setTitle(String(format: "%@ (%@)", ((self.dictData["published_batch"] as AnyObject).object(at: sender.tag) as AnyObject).value(forKey: "Batch") as? String ?? "",self.strSelectedDate), for: .normal)
                    print("Selected : \(self.strSelectedDate)")
                    self.selectedID.append(((self.dictData["published_batch"] as AnyObject).object(at: sender.tag) as AnyObject).value(forKey: "BatchID") as? String ?? "0")
                    self.publishPaper()
                
                            return
                
               })
               .setCancelButton(action: { _, _ in print("cancel")})
               .appear(originView: sender, baseViewController: self)
                   
        
    }
    
    
    
    func setDateFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "dd MMM yyyy"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        let a = dateFormatter.string(from: date!)
        return a
    }
    
    func publishPaper()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            var dictArr = [[String : Any]]()
            var dict = [String : Any]()
            for i in 0..<selectedID.count
            {
                dict["BatchID"] = selectedID[i]
                dictArr.append(dict)
            }
            
            var convertedString = String()
            do {
                let data1 =  try JSONSerialization.data(withJSONObject: dictArr, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
                convertedString = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
                print(convertedString) // <-- here is ur string
                
            } catch let myJSONError {
                print(myJSONError)
            }
            
            let parameters = [
                "MCQPlannerID" : dictData["MCQPlannerID"] as? String ?? "0" ,
                "BatchID" : convertedString ,
                "PublishDate" : strSelectedDate
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.PUBLISH_PAPER)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : NSDictionary = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data as NSDictionary
                      //  self.batchData.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            let arr = resData.value(forKey: "data") as! NSArray
                            
                            self.dictData = arr.object(at: 0) as! [String : Any]
                            self.viewDidLoad()
//                            for vc in (self.navigationController?.viewControllers ?? []) {
//                                if vc is DashboardViewController {
//                                    _ = self.navigationController?.popToViewController(vc, animated: true)
//                                    break
//                                }
//                            }
                        }
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    
}
