//
//  detailedreportcell.swift
//  TeacherApp
//
//  Created by My Mac on 21/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class detailedreportcell: UITableViewCell {

    @IBOutlet weak var lblcount: UILabel!
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var lblcorrect: UILabel!
    @IBOutlet weak var lblincorrect: UILabel!
    @IBOutlet weak var lblnotattempt: UILabel!
    @IBOutlet weak var webheightconstant: NSLayoutConstraint!
    @IBOutlet weak var textview: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
