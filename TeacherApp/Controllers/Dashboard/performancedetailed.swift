//
//  performancedetailed.swift
//  TeacherApp
//
//  Created by My Mac on 21/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class performancedetailed: UIViewController, UITableViewDataSource,UITableViewDelegate  {
    
    @IBOutlet weak var maintbl: UITableView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    var finishedLoadingInitialTableCells = false
    var MCQPlannerID = String()
    var MainArr:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imgBG.setGredient()
        maintbl.setRadius(radius: 10)

      //  maincollection.register(maindetailedcollCell.self, forCellWithReuseIdentifier: "maincell")
        maintbl.delegate = self
        maintbl.dataSource = self
        maintbl.tableFooterView = UIView()
        self.getsummary()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (MainArr).count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "maincell", for: indexPath) as! maindetailedcollCell
       // cell.layoutIfNeeded()
        //        cell.lblRadius.setRadius(radius: cell.lblRadius.frame.height/2)
        //        cell.lblRadius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        
        // cell.lblperformance.text = ((data["level_accuracy"] as AnyObject).objectAt(indexPath.row) as AnyObject).value(forKey: "LevelName") as? String
        let mydic:NSDictionary = MainArr.object(at: indexPath.row) as! NSDictionary
        print("TITLE:%@",mydic.value(forKey: "Batch") as! String)
        cell.lblperformance.setTitle((mydic.value(forKey: "Batch") as! String), for: .normal)
        cell.lblavgcount.text = String(mydic.value(forKey: "accuracy") as! Double) + "%"
       
        cell.data = mydic as! [String : Any]
        cell.innercollvw.reloadData()
      //  cell.layoutIfNeeded()
        return cell
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        //    return contentHeights[indexPath.row]
//        return UITableView.automaticDimension
//        
//        //        if contentHeights[indexPath.row] != 0.0 {
//        //            print("MYHEIGHT:",contentHeights[indexPath.row])
//        //            tblheightconstant.constant = tblquestions.contentSize.height
//        //            return contentHeights[indexPath.row]
//        //        }
//        //        return 0
//    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func getsummary()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["MCQPlannerID" : MCQPlannerID] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_SUMMARY)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : NSDictionary = [:]
                        guard let data = response.result.value as? NSDictionary,
                            let _ = data.value(forKey: "status") as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = response.result.value as! NSDictionary
//                        self.batch_list.removeAll()
//                        self.attended_list.removeAll()
//                        self.not_attended_list.removeAll()
                        
                        if resData.value(forKey: "status") as? Bool ?? false
                        {
                            self.MainArr = resData.value(forKey: "data") as! NSArray
                          //  self.MainArr = self.MainArr.object(at: 0) as! NSArray
                            print("DATA:%@", self.MainArr)

//                            self.lblAccuracyPercentage.text = "\(self.data["accuracy"] as? Int ?? 1)%"
//                            self.batch_list = self.data["batch"] as? [[String:Any]] ?? []
//                            self.attended_list = self.data["attand_student"] as? [[String:Any]] ?? []
//                            self.not_attended_list = self.data["not_attand_student"] as? [[String:Any]] ?? []
//                            self.Questions_list = self.data["Questions"] as? [[String:Any]] ?? []
//                            self.FirstTwo = self.Questions_list[1..<3].map { $0 } // ["is", "cool"]
//                            self.lblavgtime.text = String (self.data["AvarageTime"] as! Int)
//                            self.lbltotaltime.text = String (self.data["AvarageTakenTime"] as! Int)
                                self.maintbl.reloadData()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData.value(forKey: "message") as? String ?? "")
                        }
                        
//                        self.isNotAttempt = false
//                        self.collectionVw.reloadData()
//                        self.view.layoutIfNeeded()
//                        self.lblattempt.text = String(self.attended_list.count)
//                        self.lblnotattempt.text = String(self.not_attended_list.count)
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
}



