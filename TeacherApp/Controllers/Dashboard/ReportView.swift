//
//  ReportView.swift
//  TeacherApp
//
//  Created by My Mac on 05/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0

class ReportLecelCell: UICollectionViewCell {
    @IBOutlet weak var lblRadius: UILabel!
    @IBOutlet weak var lblLevel: UILabel!
}

class ReportStudentTableCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var lblcorrect: UILabel!
    @IBOutlet var lblincorrect: UILabel!
    @IBOutlet var lblnotattemt: UILabel!
    @IBOutlet var vwheight: NSLayoutConstraint!
    @IBOutlet var vwcompleted: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class ReportView: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, UITableViewDelegate , UITableViewDataSource
{
    
    @IBOutlet weak var btnTotalAttempt: UIButton!
    @IBOutlet weak var btnSelectBatch: UIButton!
    @IBOutlet weak var tblPopupBranch: UITableView!
    @IBOutlet weak var vwBranchPopup: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgGredient: UIImageView!
    
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var collectionVw: UICollectionView!
    
    @IBOutlet weak var lblAccuracyPercentage: UILabel!
    @IBOutlet weak var constTblHeight: NSLayoutConstraint!
    
    var finishedLoadingInitialCollectionCells = false
    var finishedLoadingInitialTableCells = false
    var MCQPlannerID = String()
    var BatchID = String()
    var data = [String : Any]()
    var batch_list = [[String : Any]]()
    var attended_list = [[String : Any]]()
    var not_attended_list = [[String : Any]]()
    var isNotAttempt = Bool()

    //MARK:- Web service
    
    func getReport()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["MCQPlannerID" : MCQPlannerID ,
                              "BatchID" : BatchID,
                              ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_REPORT)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.batch_list.removeAll()
                        self.attended_list.removeAll()
                        self.not_attended_list.removeAll()
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.data = resData["data"] as? [String:Any] ?? [:]
                            self.lblAccuracyPercentage.text = String(format: "%.2f%%",(self.data["accuracy"] as! NSNumber).doubleValue) //"\(self.data["accuracy"] as? Double ?? 1)%"
                            self.batch_list = self.data["batch"] as? [[String:Any]] ?? []
                            self.attended_list = self.data["attand_student"] as? [[String:Any]] ?? []
                            self.not_attended_list = self.data["not_attand_student"] as? [[String:Any]] ?? []
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.isNotAttempt = false
                        self.collectionVw.reloadData()
                        self.tblVW.reloadData()
                        self.tblPopupBranch.reloadData()
                        self.btnTotalAttempt.setTitle("Total Student Attempted(\(self.attended_list.count))", for: .normal)
                        self.view.layoutIfNeeded()
                      //  self.constTblHeight.constant = self.tblVW.contentSize.height
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        imgGredient.setGredient()
        
        vwTop.setRadius(radius: 10)
        vwBottom.setRadius(radius: 10)
        vwBranchPopup.setRadius(radius: 10)

        tblPopupBranch.tableFooterView = UIView()
        getReport()
        
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.view.layoutIfNeeded()
        constTblHeight.constant = tblVW.contentSize.height
    }

    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func lblBranchAction(_ sender: UIButton) {
        showViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
    }
    
    @IBAction func btnTotalAttendAction(_ sender: UIButton) {
        ActionSheetStringPicker.show(withTitle: "Select", rows: ["Attempted" , "Not Attempted"] as [Any], initialSelection: 0, doneBlock: {
            picker, indexes, values in
            
            if indexes == 0
            {
                self.isNotAttempt = false
                self.btnTotalAttempt.setTitle("Total Student Attempted(\(self.attended_list.count))", for: .normal)
            }
            else
            {
                self.isNotAttempt = true
                self.btnTotalAttempt.setTitle("Total Student Not Attempted(\(self.not_attended_list.count))", for: .normal)
            }
            self.tblVW.reloadData()
            self.view.layoutIfNeeded()
            self.constTblHeight.constant = self.tblVW.contentSize.height
            
//            self.selectedStandardID = self.subjectList[indexes]["StandardID"] as? String ?? ""
           
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        hideViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
    }
    
    
    //MARK:- Collection Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (data["level_accuracy"] as? [AnyObject] ?? []).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportLecelCell", for: indexPath) as! ReportLecelCell
        cell.layoutIfNeeded()
        cell.lblRadius.setRadius(radius: cell.lblRadius.frame.height/2)
        cell.lblRadius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        
        cell.lblLevel.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String
        cell.lblRadius.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/3 - 10 , height: collectionView.frame.width/3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimation()
        }
    }
    
    //MARK:- Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblPopupBranch
        {
            return batch_list.count
        }
        else
        {
            return isNotAttempt ? not_attended_list.count : attended_list.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblPopupBranch
        {
            let cell:ShareBatchListCell = tableView.dequeueReusableCell(withIdentifier: "ShareBatchListCell") as! ShareBatchListCell
            
            cell.lblRight.setRadius(radius: cell.lblRight.frame.height/2)
            cell.lblRight.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
            
            cell.lblTitle.text = batch_list[indexPath.row]["Batch"] as? String
//
            if BatchID  == batch_list[indexPath.row]["BatchID"] as? String ?? "0"
            {
                cell.lblRight.text = "✓"
            }
            else
            {
                cell.lblRight.text = ""
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell:ReportStudentTableCell = tableView.dequeueReusableCell(withIdentifier: "ReportStudentTableCell") as! ReportStudentTableCell
            cell.layoutIfNeeded()
            
            if isNotAttempt
            {
                cell.lblTitle.text = not_attended_list[indexPath.row]["StudentName"] as? String
            }
            else
            {
                cell.lblTitle.text = attended_list[indexPath.row]["StudentName"] as? String
            }
            
            cell.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblPopupBranch
        {
            BatchID = batch_list[indexPath.row]["BatchID"] as? String ?? "0"
            btnSelectBatch.setTitle(batch_list[indexPath.row]["Batch"] as? String ?? "0", for: .normal)
            hideViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
            getReport()
        }
    }
}
class SemiCirleViewAntiClockWise: UIView {
    
    var semiCirleLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if semiCirleLayer == nil {
            let arcCenter = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
            let circleRadius = bounds.size.width / 2
            let circlePath = UIBezierPath(arcCenter: arcCenter, radius: circleRadius, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 2, clockwise: false)
            
            semiCirleLayer = CAShapeLayer()
            semiCirleLayer.path = circlePath.cgPath
            semiCirleLayer.fillColor = UIColor.green.cgColor
            layer.addSublayer(semiCirleLayer)
            
            // Make the view color transparent
            backgroundColor = UIColor.clear
        }
    }
}
class SemiCirleViewClockWise: UIView {
    
    var semiCirleLayer: CAShapeLayer!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if semiCirleLayer == nil {
            let arcCenter = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
            let circleRadius = bounds.size.width / 2
            let circlePath = UIBezierPath(arcCenter: arcCenter, radius: circleRadius, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 2, clockwise: true)
            
            semiCirleLayer = CAShapeLayer()
            semiCirleLayer.path = circlePath.cgPath
            semiCirleLayer.fillColor = UIColor.lightGray.cgColor
            layer.addSublayer(semiCirleLayer)
            
            // Make the view color transparent
            backgroundColor = UIColor.clear
        }
    }
}
