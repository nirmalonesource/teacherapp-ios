//
//  NewReportView.swift
//  TeacherApp
//
//  Created by My Mac on 12/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0

class NewReportView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout , UITableViewDelegate , UITableViewDataSource,UIWebViewDelegate{

    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func lblBranchAction(_ sender: UIButton) {
      //  showViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "performancedetailed") as! performancedetailed
        nextViewController.MCQPlannerID = MCQPlannerID
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btnTotalAttendAction(_ sender: UIButton) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "Studentdetailscore") as! Studentdetailscore
        nextViewController.attended_list = attended_list
        nextViewController.not_attended_list = not_attended_list
        nextViewController.attempt = lblattempt.text ?? ""
        nextViewController.notattempt = lblnotattempt.text ?? ""
        nextViewController.batch_list = batch_list
        nextViewController.MCQPlannerID = MCQPlannerID
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @IBAction func question_click(_ sender: UIButton) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuestionView") as! QuestionView
        nextViewController.Questions_list = Questions_list
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBOutlet weak var btnSelectBatch: UIButton!
    @IBOutlet weak var vwTop: UIView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var collectionVw: UICollectionView!
    @IBOutlet weak var lblAccuracyPercentage: UILabel!
    @IBOutlet weak var roundvwtop: UIView!
    @IBOutlet weak var roundvwbottom: UIView!
    @IBOutlet weak var lblnotattempt: UILabel!
    @IBOutlet weak var lblattempt: UILabel!
    
    @IBOutlet weak var tblquestions: UITableView!
    @IBOutlet weak var lbltotaltime: UILabel!
    @IBOutlet weak var lblavgtime: UILabel!
    @IBOutlet weak var tblheightconstant: NSLayoutConstraint!
    @IBOutlet weak var vwtbl: UIView!
    @IBOutlet weak var vwtime: UIView!
    
    
    var finishedLoadingInitialCollectionCells = false
    var finishedLoadingInitialTableCells = false
    var MCQPlannerID = String()
    var BatchID = String()
    var data = [String : Any]()
    var batch_list = [[String : Any]]()
    var attended_list = [[String : Any]]()
    var not_attended_list = [[String : Any]]()
    var Questions_list = [[String : Any]]()
    var FirstTwo = [[String : Any]]()

    var isNotAttempt = Bool()
    
    func getReport()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["MCQPlannerID" : MCQPlannerID ,
                              "BatchID" : BatchID,
                              ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_REPORT)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.batch_list.removeAll()
                        self.attended_list.removeAll()
                        self.not_attended_list.removeAll()
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.data = resData["data"] as? [String:Any] ?? [:]
                            self.lblAccuracyPercentage.text = String(format: "%.2f%%",(self.data["accuracy"] as! NSNumber).doubleValue) //"\(self.data["accuracy"] as? Double ?? 1)%"
                            self.batch_list = self.data["batch"] as? [[String:Any]] ?? []
                            self.attended_list = self.data["attand_student"] as? [[String:Any]] ?? []
                            self.not_attended_list = self.data["not_attand_student"] as? [[String:Any]] ?? []
                            self.Questions_list = self.data["Questions"] as? [[String:Any]] ?? []
                            if self.Questions_list.count > 2
                            {
                                self.FirstTwo = self.Questions_list[0..<2].map { $0 } // ["is", "cool"]
                            }
                            else
                            {
                                self.FirstTwo = self.Questions_list
                            }
//                            self.lblavgtime.text = String(format: "%.0f",self.data["AvarageTime"] as? Float ?? 0)
//                            self.lbltotaltime.text = String(format: "%.0f",self.data["AvarageTakenTime"] as? Float ?? 0)
                            let AvarageTime =  String(format: "%.0f",(self.data["AvarageTime"] as! NSNumber).doubleValue)
                            let AvarageTakenTime =  String(format: "%.0f",(self.data["AvarageTakenTime"] as! NSNumber).doubleValue)
                            self.lblavgtime.text = self.stringFromTimeInterval(interval: Double(AvarageTime)!/1000 )
                            self.lbltotaltime.text = self.stringFromTimeInterval(interval: Double(AvarageTakenTime)!/1000 )
                            
                            self.tblquestions.reloadData()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.isNotAttempt = false
                        self.collectionVw.reloadData()
                        self.view.layoutIfNeeded()
                        self.lblattempt.text = String(self.attended_list.count)
                        self.lblnotattempt.text = String(self.not_attended_list.count)
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        
        if (hours > 0) {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imgBG.setGredient()
        imgGredient.setGredient()
        
        vwTop.setRadius(radius: 10)
        vwBottom.setRadius(radius: 10)
        vwtbl.setRadius(radius: 10)
        vwtime.setRadius(radius: 10)

        roundvwtop.roundCorners(corners: [.topLeft, .topRight], radius: 50)
        roundvwbottom.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 50)
        tblquestions.rowHeight = UITableView.automaticDimension
        tblquestions.estimatedRowHeight = UITableView.automaticDimension
        getReport()
        //        vwTop.roundCorners(corners: [.topLeft, .topRight], radius: 5)
        self.view.layoutIfNeeded()
    }
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (data["level_accuracy"] as? [AnyObject] ?? []).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReportLecelCell", for: indexPath) as! ReportLecelCell
        cell.layoutIfNeeded()
        cell.lblRadius.setRadius(radius: cell.lblRadius.frame.height/2)
        cell.lblRadius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        
        cell.lblLevel.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "LevelName") as? String
       // let s1:String = String(format:"%.2f",  (data[indexPath.row]["Correct"] as! NSNumber).doubleValue)
      
      // let TotalAvarage = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalAvarage") as? Float
        let TotalAvarage = (((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalAvarage") as! NSNumber).doubleValue
        
        if TotalAvarage == 0
        {
            cell.lblRadius.text = String(format: "%.0f %%", TotalAvarage )
        }
        else
        {
            let TotalCorrect = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "TotalCorrect") as? Int
            let NoOfQuestions = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
            cell.lblRadius.text = String(format: "%.1f %% \n %d-%@", TotalAvarage ,TotalCorrect ?? 0,NoOfQuestions ?? "0")
        }
       // cell.lblRadius.text = ((data["level_accuracy"] as AnyObject).object(at: indexPath.row) as AnyObject).value(forKey: "NoOfQuestions") as? String
        
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/3 - 10 , height: collectionView.frame.width/3 - 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        var lastInitialDisplayableCell = false
        if 3 > 0 && !finishedLoadingInitialCollectionCells {
            if let indexPathsForVisibleRows = collectionView.indexPathsForVisibleItems.last,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialCollectionCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialCollectionCells = true
            }
            cell.loadAnimation()
        }
    }
    
    override func viewDidLayoutSubviews() {
        tblheightconstant.constant = tblquestions.contentSize.height
    }
    func insertCSSString(into webView: UIWebView) {
        let cssString = "body {max-width:100%; width:auto !important; max-height:50%;height:50% !important; font-size: 15px; color: #fff } table { width:100% !important; color: #FFF; border-color: #FFF; border: white; border-collapse: collapse;} img{max-width:100%; width:auto !important; height:auto !important;}"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
//    func insertCSSString(into webView: UIWebView) {
//        let cssString = "body { width:100% !important; font-size: 15px; color: #000 } table { width:100% !important; color: #000; border-color: #000; border: black; border-collapse: collapse;}"
//        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
//        webView.stringByEvaluatingJavaScript(from: jsString)
//    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//       // insertCSSString(into: webView)
//        //        setNeedsLayout()
//      
//        //   webkit.scrollView.isScrollEnabled = false
//                let indexPath = IndexPath(row: webView.tag, section: 0)
//                let cell = self.tblquestions.cellForRow(at: indexPath) as! Reportcell
//        var frame = cell.webview.frame
//        print("WEBTAG:",webView.tag)
//
//        frame.size.width = cell.webview.frame.size.width
//        frame.size.height = 1
//        
//        cell.webview.frame = frame
//        frame.size.height = cell.webview.scrollView.contentSize.height
//        
//        cell.webview.frame = frame;
//       // constWebviewHeight.constant = webView.scrollView.contentSize.height + 50
//        print("TOPWEBHEIGHT:",frame)
//        //  self.layoutIfNeeded()
//       // tblList.frame.origin.y = webkit.scrollView.contentSize.height + 10
//        print("TBLFRAME:",tblquestions.frame)
//        cell.webview.resizeWebContent()
//        
//        //            webView.frame.size.height = 1;
//        //            webView.frame.size = webView.sizeThatFits(.zero)
//        
//        
//        if (contentHeights[cell.webview.tag] != 0.0)
//        {
//            // we already know height, no need to reload cell
//            return
//        }
//        print("CELLHEIGGHT:",cell.webview.scrollView.contentSize.height as Any)
//         contentHeights[cell.webview.tag] = cell.webview.scrollView.contentSize.height
//        tblquestions.reloadRows(at: [IndexPath(row: cell.webview.tag, section: 0)], with: .automatic)
//      //  layoutIfNeeded()
//        tblheightconstant.constant = tblquestions.contentSize.height
//        
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return FirstTwo.count
    }
    
    var contentHeights : [CGFloat] = [0.0,0.0,0.0,0.0,0.0]

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
            let cell:Reportcell = tableView.dequeueReusableCell(withIdentifier: "cell") as! Reportcell
        cell.layoutIfNeeded()

        cell.lblcount.text = String(indexPath.row + 1) + ")"
        cell.webview.loadHTMLString((FirstTwo[indexPath.row]["Question"] as? String)!, baseURL: nil)
        let s1:String = String(format:"%.2f%%",  (FirstTwo[indexPath.row]["Correct"] as! NSNumber).doubleValue)
        let s2:String = String(format:"%.2f%%",  (FirstTwo[indexPath.row]["InCorrect"] as! NSNumber).doubleValue)
        let s3:String = String(format:"%.2f%%",  (FirstTwo[indexPath.row]["NotAttempt"] as! NSNumber).doubleValue)
        cell.lblcorrect.text = s1
        cell.lblincorrect.text = s2
        cell.lblnotattempt.text = s3
       // cell.webview.delegate = self
           // cell.lblRight.setRadius(radius: cell.lblRight.frame.height/2)
//            cell.lblRight.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
//
//            cell.lblTitle.text = batch_list[indexPath.row]["Batch"] as? String
//            //
//            if BatchID  == batch_list[indexPath.row]["BatchID"] as? String ?? "0"
//            {
//                cell.lblRight.text = "✓"
//            }
//            else
//            {
//                cell.lblRight.text = ""
//            }
          let htmlHeight = contentHeights[indexPath.row]
            cell.webview.tag = indexPath.row
        cell.webview.frame = CGRect(x: cell.webview.frame.origin.x, y: 0, width: cell.webview.frame.size.width, height: htmlHeight)
        cell.webheightconstant.constant = cell.webview.scrollView.contentSize.height
        tblheightconstant.constant = tblquestions.contentSize.height
         cell.layoutIfNeeded()
        self.viewDidLayoutSubviews()
            cell.selectionStyle = .none
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //    return contentHeights[indexPath.row]
        return UITableView.automaticDimension

//        if contentHeights[indexPath.row] != 0.0 {
//            print("MYHEIGHT:",contentHeights[indexPath.row])
//            tblheightconstant.constant = tblquestions.contentSize.height
//            return contentHeights[indexPath.row]
//        }
//        return 0
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == tblPopupBranch
//        {
//            BatchID = batch_list[indexPath.row]["BatchID"] as? String ?? "0"
//            btnSelectBatch.setTitle(batch_list[indexPath.row]["Batch"] as? String ?? "0", for: .normal)
//            hideViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
//            getReport()
//        }
    }

}
