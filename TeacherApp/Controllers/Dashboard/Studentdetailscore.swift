//
//  Studentdetailscore.swift
//  TeacherApp
//
//  Created by My Mac on 16/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Alamofire
import SVProgressHUD

class Studentdetailscore: UIViewController, UITableViewDelegate , UITableViewDataSource {
    @IBOutlet weak var imgGredient: UIImageView!
    @IBOutlet weak var roundvwtop: UIView!
    @IBOutlet weak var roundvwbottom: UIView!
    @IBOutlet weak var lblnotattempt: UILabel!
    @IBOutlet weak var lblattempt: UILabel!
    
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var vwtbl: UIView!
    
    @IBOutlet weak var tblVW: UITableView!
    @IBOutlet weak var btnTotalAttempt: UIButton!
    
    var attended_list = [[String : Any]]()
    var batch_list = [[String : Any]]()
    var not_attended_list = [[String : Any]]()
    var isNotAttempt = Bool()
    var finishedLoadingInitialTableCells = false
    var attempt:String = ""
    var notattempt:String = ""
    var data = [String : Any]()
    var MCQPlannerID = String()
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTotalAttendAction(_ sender: UIButton) {
                ActionSheetStringPicker.show(withTitle: "Select", rows: ["Completed" , "Not Completed"] as [Any], initialSelection: 0, doneBlock: {
                    picker, indexes, values in
        
                    if indexes == 0
                    {
                        self.isNotAttempt = false
                        self.btnTotalAttempt.setTitle("Completed(\(self.attended_list.count))", for: .normal)
                    }
                    else
                    {
                        self.isNotAttempt = true
                        self.btnTotalAttempt.setTitle("Not Completed(\(self.not_attended_list.count))", for: .normal)
                    }
                    self.tblVW.reloadData()
                    self.view.layoutIfNeeded()
                   // self.constTblHeight.constant = self.tblVW.contentSize.height
        
                    //            self.selectedStandardID = self.subjectList[indexes]["StandardID"] as? String ?? ""
        
        
                    return
                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    @IBOutlet var btnfilter: UIButton!
    @IBAction func filter_click(_ sender: UIButton) {
        let arr:NSArray = batch_list as NSArray
       
        ActionSheetStringPicker.show(withTitle: "BATCH", rows:arr.value(forKey: "Batch") as? [Any], initialSelection: 0, doneBlock: {
            picker, indexes, values in
             SVProgressHUD.show(withStatus: nil)
            self.getReport(batchid: (arr.object(at: indexes) as AnyObject).value(forKey: "BatchID") as! String)
            
            return
        }, cancel: { ActionMultipleStringCancelBlock in
            print("CANCELCLICK")
            self.getReport(batchid: "")
             SVProgressHUD.show(withStatus: nil)
            return }, origin: sender)
        
    }
    
    func getReport(batchid:String)
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["MCQPlannerID" : MCQPlannerID ,
                              "BatchID" : batchid,
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_REPORT)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.batch_list.removeAll()
                        self.attended_list.removeAll()
                        self.not_attended_list.removeAll()
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.data = resData["data"] as? [String:Any] ?? [:]
//                            self.lblAccuracyPercentage.text = "\(self.data["accuracy"] as? Int ?? 1)%"
                            self.batch_list = self.data["batch"] as? [[String:Any]] ?? []
                            self.attended_list = self.data["attand_student"] as? [[String:Any]] ?? []
                            self.not_attended_list = self.data["not_attand_student"] as? [[String:Any]] ?? []
//                            self.Questions_list = self.data["Questions"] as? [[String:Any]] ?? []
//                            if self.Questions_list.count > 2
//                            {
//                                self.FirstTwo = self.Questions_list[1..<3].map { $0 } // ["is", "cool"]
//                            }
//                            else
//                            {
//                                self.FirstTwo = self.Questions_list
//                            }
//                            self.lblavgtime.text = String (self.data["AvarageTime"] as! Int)
//                            self.lbltotaltime.text = String (self.data["AvarageTakenTime"] as! Int)
                            self.tblVW.reloadData()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                       // self.isNotAttempt = false
                       // self.collectionVw.reloadData()
                      //  self.view.layoutIfNeeded()
//                        self.lblattempt.text = String(self.attended_list.count)
//                        self.lblnotattempt.text = String(self.not_attended_list.count)
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
          imgGredient.setGredient()
      //   vwBottom.setRadius(radius: 10)
        tblVW.tableFooterView = UIView()
        vwBottom.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        vwBottom.layer.borderColor = UIColor.lightGray.cgColor
        vwtbl.layer.borderColor = UIColor.lightGray.cgColor
        vwBottom.layer.borderWidth = 0.5
        vwtbl.layer.borderWidth = 0.5
        roundvwtop.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        roundvwbottom.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
        lblattempt.text = attempt
        lblnotattempt.text = notattempt

    }
    
    override func viewDidLayoutSubviews() {
        
    }
    //MARK:- Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
       
            return isNotAttempt ? not_attended_list.count : attended_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
       
            let cell:ReportStudentTableCell = tableView.dequeueReusableCell(withIdentifier: "ReportStudentTableCell") as! ReportStudentTableCell
            cell.layoutIfNeeded()
            
            if isNotAttempt
            {
                let StudentName = not_attended_list[indexPath.row]["StudentName"] as? String
                let BatchName = not_attended_list[indexPath.row]["BatchName"] as? String
                cell.lblTitle.text = String(format: "%@ ( %@ )",StudentName ?? "", BatchName ?? "")
                cell.vwheight.constant = 0;
                cell.vwcompleted.isHidden = true
            }
            else
            {
                cell.vwcompleted.isHidden = false
                let StudentName = attended_list[indexPath.row]["StudentName"] as? String
                let BatchName = attended_list[indexPath.row]["BatchName"] as? String
                cell.lblTitle.text = String(format: "%@ ( %@ )",StudentName ?? "", BatchName ?? "")
                
              let TotalRight =  attended_list[indexPath.row]["TotalRight"] as? String
                let TotalWrong =  attended_list[indexPath.row]["TotalWrong"] as? String
                let TotalQuestion =  attended_list[indexPath.row]["TotalQuestion"] as? String
                 let TotalAttempt =  attended_list[indexPath.row]["TotalAttempt"] as? String
                let notanswered:Int = (Int(TotalQuestion!) ?? 0) - (Int(TotalAttempt!) ?? 0)
                
                cell.lblcorrect.text = String(format: "%@  -  %@",TotalRight ?? "", TotalQuestion ?? "")
                cell.lblincorrect.text = String(format: "%@  -  %@",TotalWrong ?? "", TotalQuestion ?? "")
                cell.lblnotattemt.text = String(format: "%d  -  %@",notanswered, TotalQuestion ?? "")
                
                cell.vwheight.constant = 70;
            }
            
            cell.layoutIfNeeded()
            cell.selectionStyle = .none
            return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == tblPopupBranch
//        {
//            BatchID = batch_list[indexPath.row]["BatchID"] as? String ?? "0"
//            btnSelectBatch.setTitle(batch_list[indexPath.row]["Batch"] as? String ?? "0", for: .normal)
//            hideViewWithAnimation(vw: vwBranchPopup, img: imgBlur)
//            getReport()
//        }
        
        if isNotAttempt
        {
            
        }
        else
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuestionAnswerView") as! QuestionAnswerView
            nextViewController.PlannerID =  MCQPlannerID
            nextViewController.StudentID =  attended_list[indexPath.row]["StudentID"] as? String ?? ""
            nextViewController.Name =  attended_list[indexPath.row]["StudentName"] as? String ?? ""
            nextViewController.isfrom = "summary"
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }

}
