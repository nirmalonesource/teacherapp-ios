//
//  QueryView.swift
//  TeacherApp
//
//  Created by My Mac on 24/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class QueryView: UIViewController {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lblPhoneNo: UILabel!
    @IBOutlet weak var lblSupport: UILabel!
    @IBOutlet weak var txtFeedback: UITextField!
    @IBOutlet weak var imgSubmitBG: UIImageView!
    @IBOutlet weak var btnSend: UIButton!
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        btnSend.setRadius(radius: btnSend.frame.height/2)
        imgSubmitBG.setRadius(radius: imgSubmitBG.frame.height/2)
        btnSend.setGredientText()
        btnSend.dampAnimation()
        imgLogo.setRadius(radius: imgLogo.frame.height/2)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(QueryView.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.imgLogo.isUserInteractionEnabled = true
        self.imgLogo.clipsToBounds = true
        self.imgLogo.addGestureRecognizer(tapGesture)
        
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        callNumber(phoneNumber: "7710012112")
    }
    
    private func callNumber(phoneNumber:String) {
        
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
    
    /*
     let parameters = ["product_id" : "\(editProduct_id)" ,
     "user_id" : UserDefaults.standard.getUserDict()["user_id"]!
     ] as [String : Any]
  */
    
    func queryApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            //let DeviceID = UIDevice.current.identifierForVendor?.uuidString
            //print("DeviceID: \(DeviceID!)") "DeviceID": DeviceID!
        
            let parameters = ["Message" : txtFeedback.text!] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                            "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                            "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                            "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.QUERYMESSAGE)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                       
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func Validation()
    {
        
        self.view.endEditing(true)
     
        if txtFeedback.text == ""
        {
         showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.MESSAGE, isTwoButton: false)
         }
     
        else {
          queryApi()
        }
     
     }
    
    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        Validation()
    }
    
}
