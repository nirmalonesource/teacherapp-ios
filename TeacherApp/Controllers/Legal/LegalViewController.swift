//
//  LegalViewController.swift
//  TeacherApp
//
//  Created by archana on 24/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import SVProgressHUD

 struct Legal {
    var Name: String?
    var value: String?
    
    init(Name: String, value: String) {
        self.Name = Name
        self.value = value
    }
}

class LegalListCell: UITableViewCell {
    
    @IBOutlet weak var vwBG: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class LegalViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    
    var finishedLoadingInitialTableCells = false
    var selectedIndex = Int()
    var dataToDisplay: [Legal] = []
    var dictListingData = [[String : Any]]()
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resendApi()
        imgBG.setGredient()
        
        tblVW.setRadius(radius: 10)
        tblVW.tableFooterView = UIView()
        
        self.view.layoutIfNeeded()
        
    }
    
    func resendApi()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            SVProgressHUD.show(withStatus: nil)
           // let DeviceID = UIDevice.current.identifierForVendor?.uuidString
           // print("DeviceID: \(DeviceID!)")
           
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.LEGAL)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                        
                            self.dictListingData = resData["data"] as! [[String : Any]]
                            for i in 0..<self.dictListingData.count {
                            let Name = self.dictListingData[i]["Name"] as? String ?? ""
                            let value = self.dictListingData[i]["value"] as? String ?? ""
                            self.dataToDisplay.append(Legal(Name: Name, value: value))
                                
                            }
            
                        }
                        
                        showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        
                        DispatchQueue.main.async {
                             self.tblVW.reloadData()
                        }
                        
                       // self.tblVW.reloadData()
                        
                        self.view.layoutIfNeeded()
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:LegalListCell = tableView.dequeueReusableCell(withIdentifier: "LegalListCell") as! LegalListCell
        cell.layoutIfNeeded()
        
        cell.lblTitle.text = dataToDisplay[indexPath.row].Name
        cell.lblSubtitle.text = dataToDisplay[indexPath.row].value
        
        if indexPath.row == 0
        {
//            cell.vwBG.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        }
        if indexPath.row == 3
        {
//            cell.vwBG.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
        }
        
        cell.lblSubtitle.numberOfLines = selectedIndex == indexPath.row ? 0 : 1
        
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblVW.reloadData()
        
        let cell : LegalListCell = (tblVW.cellForRow(at: indexPath) as? LegalListCell)!
        cell.lblSubtitle.fadeIn()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 4 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    //MARK:- Button action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

