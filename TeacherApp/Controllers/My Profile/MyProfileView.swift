//
//  MyProfileView.swift
//  TeacherApp
//
//  Created by archana on 31/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ProfileMainCell: UICollectionViewCell  {
    
    @IBOutlet var lblCategoryName: UILabel!
    @IBOutlet var lblSelected: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
}

class ProfileSubTableCell: UITableViewCell {
    
    @IBOutlet var lblCategoryName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class MyProfileView: UIViewController , UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout , UITableViewDelegate , UITableViewDataSource
{

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var vwPopupTop: UIView!
    
    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var lblUserNam: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var vwCategory: UIView!
    
    @IBOutlet weak var collectionVW: UICollectionView!
    @IBOutlet weak var tblVW: UITableView!
    var selectedIndex = Int()
    var dataProfile = [String : Any]()
    var dataBranch = [[String : Any]]()
    var selectedBranchData = [String : Any]()
    var finishedLoadingInitialTableCells = false

    //MARK:- Web Service
    
    func getProfile()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
           
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PROFILE)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            self.dataProfile = resData["data"] as? [String:Any] ?? [:]
                            self.lblUserNam.text = "\(self.dataProfile["firstname"] as? String ?? "")" + "\(self.dataProfile["lastname"] as? String ?? "")"
                            self.lblEmail.text = self.dataProfile["email"] as? String ?? ""
                            self.lblMobileNo.text = self.dataProfile["mobileno"] as? String ?? ""
                            self.dataBranch = self.dataProfile["branch"] as? [[String : Any]] ?? []
                            
                            self.selectedBranchData = self.dataBranch[0]
                            
                            self.collectionVW.reloadData()
                            self.tblVW.reloadData()
                            
//                            let img = self.dataProfile["Image"] as? String ?? ""
//                            self.imgUser.sd_setImage(with: URL(string : (img.URLQueryAllowedString()!)), placeholderImage : nil , options: .progressiveDownload,
//                                                   completed: nil)

                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layoutIfNeeded()
        imgBG.setGredient()
        
        vwPopupTop.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        vwCategory.roundCorners(corners: [.bottomLeft , .bottomRight], radius: 10)
        
        imgUser.setRadius(radius: imgUser.frame.height/2)
        imgUser.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.lightGreen))
        
        let rightSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight(swipeGestureRecognizer:)))
        rightSwipeGestureRecognizer.direction =  UISwipeGestureRecognizer.Direction.right
        tblVW.addGestureRecognizer(rightSwipeGestureRecognizer)
        
        let leftSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft(swipeGestureRecognizer:)))
        leftSwipeGestureRecognizer.direction =  UISwipeGestureRecognizer.Direction.left
        tblVW.addGestureRecognizer(leftSwipeGestureRecognizer)
        
        getProfile()
        
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func btnMenuAction(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
        self.addChild(popOverVC)
        let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = sd
        //self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
    
    //MARK:- Collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataBranch.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileMainCell", for: indexPath) as! ProfileMainCell
        cell.layoutIfNeeded()
        cell.imgBG.setGredient()
        
        cell.lblCategoryName.text = dataBranch[indexPath.row]["BranchName"] as? String
        if indexPath.row == selectedIndex
        {
            cell.lblSelected.backgroundColor = UIColor.white
        }
        else
        {
            cell.lblSelected.backgroundColor = UIColor.clear
        }
        
        cell.layoutIfNeeded()
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.width/2, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row != selectedIndex
        {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            if indexPath.row < selectedIndex
            {
                transition.subtype = CATransitionSubtype.fromLeft
            }
            else
            {
                transition.subtype = CATransitionSubtype.fromRight
            }
            tblVW.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            
            selectedIndex = indexPath.row
            selectedBranchData = dataBranch[selectedIndex]
            tblVW.reloadData()
            collectionVW.reloadData()
            collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }
        
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var myCustomView: DateHeaderView?
        myCustomView = Bundle.main.loadNibNamed("DateHeaderView", owner: self, options: nil)?.first as? DateHeaderView
        myCustomView?.lblDate.text = ((selectedBranchData["standard"] as AnyObject).object(at: section) as AnyObject).value(forKey: "StandardName") as? String ?? ""

        myCustomView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 40)
        
        return myCustomView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        guard let option_list = selectedBranchData["standard"] as? [Any] else {
            return 0
        }
        return option_list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let option_list = ((selectedBranchData["standard"] as AnyObject).object(at: section) as AnyObject).value(forKey: "Subject") as? [[String:Any]] else {
            return 0
        }
        return option_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ProfileSubTableCell = tableView.dequeueReusableCell(withIdentifier: "ProfileSubTableCell") as! ProfileSubTableCell
        
        let arr = ((selectedBranchData["standard"] as AnyObject).object(at: indexPath.section) as AnyObject).value(forKey: "Subject") as? [[String:Any]] ?? []
        
        cell.lblCategoryName.text = arr[indexPath.row]["SubjectName"] as? String ?? ""
        cell.selectionStyle = .none
        return cell
       
    }
    
    //MARK:- Gesture
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool
    {
        return true
    }
    
    @objc func swipeLeft(swipeGestureRecognizer: UISwipeGestureRecognizer)
    {
        if selectedIndex+1 < dataBranch.count
        {
            selectedIndex += 1
            setScroll(index: selectedIndex , isLeft: true)
        }
    }
    
    @objc func swipeRight(swipeGestureRecognizer: UISwipeGestureRecognizer)
    {
        if selectedIndex != 0
        {
            selectedIndex -= 1
            setScroll(index: selectedIndex , isLeft: false)
        }
    }
    
    func setScroll(index : Int , isLeft : Bool){
       
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.fillMode = CAMediaTimingFillMode.forwards
        transition.duration = 0.5
        if isLeft
        {
            transition.subtype = CATransitionSubtype.fromRight
        }
        else{
            transition.subtype = CATransitionSubtype.fromLeft
        }
        tblVW.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
        selectedIndex = index
        selectedBranchData = dataBranch[selectedIndex]
        let indexPath = IndexPath(item: index, section: 0)
        
        tblVW.reloadData()
        collectionVW.reloadData()
        collectionVW.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}
