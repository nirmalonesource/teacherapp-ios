//
//  LoginView.swift
//  TeacherApp
//
//  Created by My Mac on 23/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class LoginView: UIViewController , UITextFieldDelegate {

    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var imgSubmitBG: UIImageView!
    @IBOutlet weak var txtPhone: UITextField!
    
    //MARK:- View Life Cyycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        btnSubmit.setRadius(radius: btnSubmit.frame.height/2)
        imgLogo.setRadius(radius: imgLogo.frame.height/2)
        imgSubmitBG.setRadius(radius: imgSubmitBG.frame.height/2)
        btnSubmit.setGredientText()
        btnSubmit.dampAnimation()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
   //MARK:- WEB SERVICES
    
    func getlogin()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
           SVProgressHUD.show(withStatus: nil)
            let DeviceID = UIDevice.current.identifierForVendor?.uuidString
            print("DeviceID: \(DeviceID!)")
            let parameters = ["MobileNo" : txtPhone.text!,
                              "DeviceID": DeviceID!] as [String : Any]
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY]
           
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.LOGIN)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                           return
                    }
                        resData = data
                        if resData["status"] as? Bool ?? false
                        {
                            var dict = resData["data"] as? [String : Any] ?? [:]
                            let otp = dict["otp"] as? Int ?? 0
                            print(" otp is: \(otp)")
                            if otp != 0 {
                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OTPVerificationView") as! OTPVerificationView
                                nextViewController.otpGet = otp
                                nextViewController.mobileNoGet = self.txtPhone.text!
                                self.navigationController?.pushViewController(nextViewController, animated: true)
                            }
                                
                            else {
                               print("Please Enter otp number")
                            }
                        }
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                    
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
             }
         }
    }
    
    
    //MARK:- Button Action
    
    func validation()
    {
        self.view.endEditing(true)
        
        if txtPhone.text == ""
        {
            showAlert(uiview: self, msg: ConstantVariables.LocalizeString.PLEASE_ENTER + " " + ConstantVariables.LocalizeString.OTP, isTwoButton: false)
        }
            
        else {
            getlogin()
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: UIButton) {
        validation()
    }
    
    //MARK:- Textfield Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


 
