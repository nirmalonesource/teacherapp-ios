//
//  SearchViewController.swift
//  TeacherApp
//
//  Created by archana on 24/01/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0
import SwiftyPickerPopover

class SearchListCell: UITableViewCell {
    
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var lblPaperTitle: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

class SearchViewController: UIViewController , UITableViewDelegate , UITableViewDataSource
{
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var vwFilter: UIView!
    
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var btnSelectStandard: UIButton!
    @IBOutlet weak var btnSelectSubject: UIButton!
    
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var imgSaveBG: UIImageView!
    var finishedLoadingInitialTableCells = false
    var paperList = [[String : Any]]()
    var subjectList = [[String : Any]]()
    var selectedStandardIndex = Int()
    var selectedStandardID = String()
    var selectedSubjectID = String()
    var strSelectedDate = String()
    
    //MARK:- Web service
    
    func getParerList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["Date" : strSelectedDate ,
                              "Month" : "",
                              "StandardID" : selectedStandardID ,
                              "SubjectID" : selectedSubjectID ,

                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PAPER_LIST)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.paperList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.paperList = resData["data"]!["paper_info"] as? [[String:Any]] ?? []
                            self.hideViewWithAnimation(vw: self.vwFilter, img: self.imgBlur)
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        self.tblVW.reloadData()
                        self.view.layoutIfNeeded()
                        
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func getSubjectList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_SUBJECT_LIST)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.subjectList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.subjectList = resData["data"] as? [[String:Any]] ?? []
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        imgBG.setGredient()
        tblVW.tableFooterView = UIView()
        self.view.layoutIfNeeded()
        getParerList()
        getSubjectList()
        
        vwFilter.setRadius(radius: 10)
        btnSelectDate.setRadiusBorder(color: UIColor.lightGray)
        btnSelectSubject.setRadiusBorder(color: UIColor.lightGray)
        btnSelectStandard.setRadiusBorder(color: UIColor.lightGray)
        btnReset.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
        
        btnSelectDate.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        btnSelectSubject.roundCorners(corners: [.topLeft , .topRight], radius: 10)
        btnSelectStandard.roundCorners(corners: [.topLeft , .topRight], radius: 10)

        btnReset.setRadius(radius: 10)
        btnSave.setRadius(radius: 10)
        imgSaveBG.setGredient()
        imgSaveBG.setRadius(radius: 10)
        
        self.view.layoutIfNeeded()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addTab(strView: "Search")
    }
    
    //MARK:- Tableview methods
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var myCustomView: DateHeaderView?
        myCustomView = Bundle.main.loadNibNamed("DateHeaderView", owner: self, options: nil)?.first as? DateHeaderView
        myCustomView?.frame = CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH, height: 40)
        
        return myCustomView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paperList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SearchListCell = tableView.dequeueReusableCell(withIdentifier: "SearchListCell") as! SearchListCell
        
        cell.lblPaperTitle.text = paperList[indexPath.row]["PlannerName"] as? String
        cell.lblSubTitle.text = "\(paperList[indexPath.row]["SubjectName"] as? String ?? "") | " + "\(paperList[indexPath.row]["StandardName"] as? String ?? "") | " + "\(paperList[indexPath.row]["MediumName"] as? String ?? "")" + "(\(paperList[indexPath.row]["BoardName"] as? String ?? ""))"
        
        let df = paperList[indexPath.row]["EndDate"] as? String ?? ""
        let getDate = setDateWithFormat(strDate: df)
        cell.lblTime.text = getDate
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyPlanViewController") as! MyPlanViewController
        nextViewController.dictData = paperList[indexPath.row]
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if paperList.count > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd ,yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }

    //MARK:- Button Action
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
        self.addChild(popOverVC)
        let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = sd
        //self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
    
    @IBAction func btnCloseAction(_ sender: UIButton) {
        hideViewWithAnimation(vw: vwFilter, img: imgBlur)
    }
    
    @IBAction func btnSelectDateAction(_ sender: UIButton) {
        self.view.endEditing(true)
//        let datePicker = ActionSheetDatePicker(title: "Select Date:", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
//            picker, value, index in
//
//            let dateFormatterGet = DateFormatter()
//            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MMM dd , yyyy"
//
//            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
//            print(dateFormatter.string(from: date!))
//            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
//
//            let dateFormatter1 = DateFormatter()
//            dateFormatter1.dateFormat = "yyyy/MM/dd"
//
//            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
//            self.strSelectedDate = (dateFormatter1.string(from: date1!))
//            print("Selected : \(self.strSelectedDate)")
//            return
//        }, cancel: { ActionStringCancelBlock in return },
//           origin: sender.superview!.superview)
//        datePicker?.maximumDate = Date()
//        datePicker?.show()
        
        DatePickerPopover(title: "Select Date")
                .setDateMode(.date)
                //.setMaximumDate(Date())
                .setSelectedDate(Date())
                .setDoneButton(action: {
                    popover, selectedDate in
                    
                    print("selectedDate \(selectedDate)")
                    let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy"
                   sender.setTitle("\(formatter.string(from: selectedDate))", for: .normal)
                  
                   let dateFormatter1 = DateFormatter()
                      dateFormatter1.dateFormat = "yyyy-MM-dd"
                  self.strSelectedDate = (dateFormatter1.string(from: selectedDate))
                      print("Selected : \(self.strSelectedDate)")
                })
                .setCancelButton(action: { _, _ in print("cancel")})
                .appear(originView: sender, baseViewController: self)
    }
    
    @IBAction func btnSelectStandardAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if subjectList.count != 0
        {
            ActionSheetStringPicker.show(withTitle: "Select Standard", rows: subjectList.map({ $0["StandardName"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                picker, indexes, values in
                self.btnSelectStandard.setTitle("\(values!)", for: .normal)
                self.selectedStandardID = self.subjectList[indexes]["StandardID"] as? String ?? ""
//
                self.btnSelectSubject.setTitle("Select Subject", for: .normal)
                self.selectedSubjectID = ""

//                self.btnState.setTitle("State", for: .normal)
//                self.btnCity.setTitle("City", for: .normal)
//                self.strStateId = ""
//                self.strCityId = ""
                
                return
            }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        }
    }
    
    @IBAction func btnSelectSubjectAction(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if self.btnSelectStandard.currentTitle != "Select Standard"
        {
          
            let arr = subjectList[selectedStandardIndex]["Subject"] as? [[String:Any]] ?? []
            if arr.count != 0
            {
                ActionSheetStringPicker.show(withTitle: "Select Subject", rows: arr.map({ $0["SubjectName"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                    picker, indexes, values in
                    self.btnSelectSubject.setTitle("\(values!)", for: .normal)
                    self.selectedSubjectID = arr[indexes]["SubjectID"] as? String ?? ""
                    return
                }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
            }
        }
            
        else {
            print("Please Select Standard")
        }
    }
    
    @IBAction func btnResetAction(_ sender: UIButton) {
        btnSelectStandard.setTitle("Select Standard", for: .normal)
        selectedStandardID = ""
        
        btnSelectSubject.setTitle("Select Subject", for: .normal)
        selectedSubjectID = ""
        
        btnSelectDate.setTitle("Select Date", for: .normal)
        strSelectedDate = ""
    }
    
    @IBAction func btnSaveAction(_ sender: UIButton) {
        getParerList()
    }
    
    @IBAction func btnFilterAction(_ sender: UIButton) {
        showViewWithAnimation(vw: vwFilter, img: imgBlur)
    }
    
}
