//
//  QuestionAnswerCell.swift
//  TeacherApp
//
//  Created by My Mac on 13/09/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class QuestionAnswerCell: UITableViewCell {
    
    @IBOutlet var lblcount: UILabel!
    @IBOutlet var txtv_question: UITextView!
    @IBOutlet var btnquestionimg: UIButton!
    
    @IBOutlet var btnA: UIButton!
    @IBOutlet var txtv_optionA: UITextView!
    @IBOutlet var btnoptionAimg: UIButton!
    
    @IBOutlet var btnB: UIButton!
    @IBOutlet var txtv_optionB: UITextView!
    @IBOutlet var btnoptionBimg: UIButton!
    
    @IBOutlet var btnC: UIButton!
    @IBOutlet var txtv_optionC: UITextView!
    @IBOutlet var btnoptionCimg: UIButton!
    
    @IBOutlet var btnD: UIButton!
    @IBOutlet var txtv_optionD: UITextView!
    @IBOutlet var btnoptionDimg: UIButton!
    
    @IBOutlet var optionAheight: NSLayoutConstraint!
    @IBOutlet var optionBheight: NSLayoutConstraint!
    @IBOutlet var optionCheight: NSLayoutConstraint!
    @IBOutlet var optionDheight: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnA.layer.cornerRadius = 15;
        btnB.layer.cornerRadius = 15;
        btnC.layer.cornerRadius = 15;
        btnD.layer.cornerRadius = 15;
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
