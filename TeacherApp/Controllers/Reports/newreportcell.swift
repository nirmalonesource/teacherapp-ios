//
//  newreportcell.swift
//  TeacherApp
//
//  Created by My Mac on 23/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class newreportcell: UITableViewCell {
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblsubject: UILabel!
    @IBOutlet weak var lblstartdate: UILabel!
    @IBOutlet weak var lblenddate: UILabel!
    @IBOutlet weak var progressview: UIProgressView!
    @IBOutlet weak var lblpercentage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
