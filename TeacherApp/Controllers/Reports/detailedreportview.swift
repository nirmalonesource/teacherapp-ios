//
//  detailedreportview.swift
//  TeacherApp
//
//  Created by My Mac on 24/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//


import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0

class detailedreportview: UIViewController, UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var imgBlur: UIImageView!

    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    var MainArr:NSArray = []
    var MainArrCopy:NSArray = []
   // var MainDict:NSDictionary = [:]
    var finishedLoadingInitialTableCells = false
    var strtitle:String = ""
    var isfilterclicked = Bool()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgBlur.setGredient()
        tbllist.roundCorners(corners: [.topLeft,.topRight,], radius: 10)
        lbltitle.text = strtitle
        tbllist.tableFooterView = UIView()
        MainArrCopy = MainArr
        print("MAINARR:%@",MainArr)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return MainArr.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! newreportcell
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderColor = UIColor.lightGray.cgColor
//        let AssignmentPending:NSArray = MainDict.value(forKey: "AssignmentPending") as! NSArray
//        let AssignmentFuture:NSArray = MainDict.value(forKey: "AssignmentFuture") as! NSArray
//        let AssignmentMissed:NSArray = MainDict.value(forKey: "AssignmentMissed") as! NSArray
        //  let StudentInfo:NSArray = MainDict.value(forKey: "StudentInfo") as! NSArray
        
        if strtitle == "Student Report"
        {
            cell.lblsubject.isHidden = true
            cell.lblstartdate.isHidden = true
            cell.lblenddate.isHidden = true
            
            let StudentName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentName") as? String
            let BatchName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "BatchName") as? String
            let BranchName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "BranchName") as? String
            let part1 = StudentName! + " (" + BatchName! + " , "
            let part2 = part1 + BranchName! + ")"
            cell.lblname.text = part2
            cell.lblname.sizeToFit()
            
            cell.lblpercentage.text = String(((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentAccuracy") as? NSString ?? "0") + "%"
            
            let StudentAccuracy = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentAccuracy") as? String
            if StudentAccuracy != nil
            {
                cell.progressview.progress = Float(StudentAccuracy ?? "")! / 100
            }
            
        }
        else
        {
            cell.progressview.isHidden = true
            cell.lblpercentage.isHidden = true
            cell.lblname.text = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as? String
            
            let plnrname = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "SubjectName") as? String
            let StandardName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StandardName") as? String
            let MediumName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "MediumName") as? String
            let BoardName = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "BoardName") as? String
            let part1 = plnrname! + " | " + StandardName!
            let part2 = part1 + " | " + MediumName!
            let part3 = part2 + ".(" + BoardName! + ")"
            cell.lblsubject.text = part3
            
            let StartDate = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StartDate") as? String
            let EndDate = ((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "EndDate") as? String
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            
            if let date = dateFormatterGet.date(from: StartDate!) {
                cell.lblstartdate.text = "Start Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            if let date = dateFormatterGet.date(from: EndDate!) {
                cell.lblenddate.text = "End Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            
        }
      
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if strtitle == "Student Report"
        {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "chartview") as! chartview
            nextViewController.strtitle =  (((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentName") as? String)!
            nextViewController.student_id =  (((MainArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentID") as? String)!
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
 
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    var Questions_list = [[String : Any]]()
    @IBAction func sort_click(_ sender: UIButton) {
        
        MainArr = MainArrCopy
        
        if isfilterclicked
        {
            isfilterclicked = false
            let newArr = MainArr.sortedArray(using: [NSSortDescriptor(key: "StudentAccuracy", ascending: false)]) as NSArray
            MainArr = newArr
        }
        else
        {
            isfilterclicked = true
            let newArr = MainArr.sortedArray(using: [NSSortDescriptor(key: "StudentAccuracy", ascending: true)]) as NSArray
            MainArr = newArr
        }
        
        tbllist.reloadData()
    }
    @IBAction func filter_click(_ sender: UIButton) {
        var arr:NSArray = []
        let filterarr:NSMutableArray = []
        let userinfo:NSDictionary = (UserDefaults.standard.getUserDict()["teacher"] as AnyObject) as! NSDictionary
        arr = userinfo.value(forKey: "batch") as! NSArray
        
        
        ActionSheetStringPicker.show(withTitle: "BATCH", rows:arr.value(forKey: "BatchName") as? [Any], initialSelection: 0, doneBlock: {
            picker, indexes, values in
            SVProgressHUD.show(withStatus: nil)
           // self.getReport(batchid: (arr.object(at: indexes) as AnyObject).value(forKey: "BatchID") as! String)
            for index in 0..<self.MainArrCopy.count {
                
                if ((self.MainArrCopy.object(at: index) as AnyObject).value(forKey: "BatchID") as! String) == ((arr.object(at: indexes) as AnyObject).value(forKey: "BatchID") as! String)
                {
                   // filterarr.addingObjects(from: [self.MainArr.object(at: index)])
                    filterarr.addObjects(from: [self.MainArrCopy.object(at: index)])
                }
            }
            self.MainArr = filterarr;
            self.tbllist.reloadData()
            return
        }, cancel: { ActionMultipleStringCancelBlock in
            print("CANCELCLICK")
            self.MainArr = self.MainArrCopy
            self.tbllist.reloadData()
          //  self.getReport(batchid: "")
            SVProgressHUD.show(withStatus: nil)
            return }, origin: sender)
        
    }
    
}
