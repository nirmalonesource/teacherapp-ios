//
//  QuestionAnswerView.swift
//  TeacherApp
//
//  Created by My Mac on 12/09/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class QuestionAnswerView: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var imgGredient: UIImageView!
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var lblheadertitle: UILabel!
    @IBOutlet var btnaccuracy: UIButton!
    
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lbltimetaken: UILabel!
    @IBOutlet var lblcorrect: UILabel!
    @IBOutlet var lblincorrect: UILabel!
    @IBOutlet var lblnotattempt: UILabel!
    
    @IBOutlet var tbllist: UITableView!
    @IBOutlet var topvwheight: NSLayoutConstraint!
    
    @IBOutlet var tblheight: NSLayoutConstraint!
    
    var finishedLoadingInitialTableCells = false
    var PlannerID = String()
    var StudentID = String()
    var StudentMCQTestID = String()
     var Name = String()
     var isfrom = String()
    var percentage = "1.0"
    
    var data = [String : Any]()
    var SummaryArr:NSMutableArray = []
    var TestDetailDict:NSMutableDictionary = [:]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgGredient.setGredient()
        tbllist.rowHeight = UITableView.automaticDimension
        tbllist.estimatedRowHeight = UITableView.automaticDimension
        tbllist.setRadius(radius: 10)
        if isfrom == "summary"
        {
            btnaccuracy.isHidden = true
            Student_summary()
        }
        else
        {
            Student_Planner()
            btnaccuracy.isHidden = false
            topvwheight.constant = 0
            btnaccuracy.setTitle(percentage, for: .normal)
        }
    }
    
    //MARK:- Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return SummaryArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:QuestionAnswerCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! QuestionAnswerCell
        cell.layoutIfNeeded()
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.lblcount.text = String(indexPath.row+1) + ")"
        
        let htmlQ = (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "Question") as? String
         cell.txtv_question.attributedText = htmlQ?.htmlAttributed(family: "Helvetica", size: 14, color: UIColor.init(red: 0, green: 0, blue: 0))
        
        let IsCorrect = (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "IsCorrect") as? String
        
        if percentage != "0.0"{
            if IsCorrect == "1"
            {
                cell.btnquestionimg.setImage(UIImage(named: "correct"), for: .normal)
            }
            else
            {
                cell.btnquestionimg.setImage(UIImage(named: "incorrect"), for: .normal)
            }
        }
        else
        {
             cell.btnquestionimg.setImage(UIImage(named: ""), for: .normal)
        }
        
        
        
        tblheight.constant = tableView.contentSize.height;
        
        let optionarr:NSArray = (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "Options") as? NSArray ?? []
        
        if optionarr.count > 0 {
            
            let htmlA = (optionarr.object(at: 0) as AnyObject).value(forKey: "Options") as? String
           
            cell.optionAheight.constant = 50;
            if (optionarr.object(at: 0) as AnyObject).value(forKey: "IsCorrect") as? String == "1"
            {
                cell.txtv_optionA.attributedText = htmlA?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 144, blue: 81))
                cell.btnA.titleLabel?.textColor = UIColor.green
                cell.btnoptionAimg.setImage(UIImage(named: "correct"), for: .normal)
                cell.btnoptionAimg.isHidden = false
            }
            else
            {
                cell.txtv_optionA.attributedText = htmlA?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 0, blue: 0))
                cell.btnA.titleLabel?.textColor = UIColor.black
                cell.btnoptionAimg.isHidden = true
            }
           
             if (optionarr.object(at: 0) as AnyObject).value(forKey: "MCQOptionID") as? String == (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "AnswerID") as? String
            {
                if (optionarr.object(at: 0) as AnyObject).value(forKey: "IsCorrect") as? String == "0" && percentage != "0.0"
                {
                    cell.txtv_optionA.attributedText = htmlA?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 255, green: 38, blue: 0))
                    cell.btnA.titleLabel?.textColor = UIColor.red
                }
            }
        }
        else
        {
            cell.optionAheight.constant = 0;
        }
        
        if optionarr.count > 1 {
            
            let htmlB = (optionarr.object(at: 1) as AnyObject).value(forKey: "Options") as? String
            
            cell.optionAheight.constant = 50;
            if (optionarr.object(at: 1) as AnyObject).value(forKey: "IsCorrect") as? String == "1"
            {
                cell.txtv_optionB.attributedText = htmlB?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 144, blue: 81))
                cell.btnB.titleLabel?.textColor = UIColor.green
                cell.btnoptionBimg.setImage(UIImage(named: "correct"), for: .normal)
                cell.btnoptionBimg.isHidden = false
            }
            else
            {
                cell.txtv_optionB.attributedText = htmlB?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 0, blue: 0))
                cell.btnB.titleLabel?.textColor = UIColor.black
                cell.btnoptionBimg.isHidden = true
            }
            
            if (optionarr.object(at: 1) as AnyObject).value(forKey: "MCQOptionID") as? String == (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "AnswerID") as? String
            {
                if (optionarr.object(at: 1) as AnyObject).value(forKey: "IsCorrect") as? String == "0" && percentage != "0.0"
                {
                    cell.txtv_optionB.attributedText = htmlB?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 255, green: 38, blue: 0))
                    cell.btnB.titleLabel?.textColor = UIColor.red
                }
            }
        }
        else
        {
            cell.optionBheight.constant = 0;
        }
        
        
        if optionarr.count > 2 {
            
            let htmlC = (optionarr.object(at: 2) as AnyObject).value(forKey: "Options") as? String
            
            cell.optionCheight.constant = 50;
            if (optionarr.object(at: 2) as AnyObject).value(forKey: "IsCorrect") as? String == "1"
            {
                cell.txtv_optionC.attributedText = htmlC?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 144, blue: 81))
                cell.btnC.titleLabel?.textColor = UIColor.green
                cell.btnoptionCimg.setImage(UIImage(named: "correct"), for: .normal)
                cell.btnoptionCimg.isHidden = false
            }
            else
            {
                cell.txtv_optionC.attributedText = htmlC?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 0, blue: 0))
                cell.btnC.titleLabel?.textColor = UIColor.black
                cell.btnoptionCimg.isHidden = true
            }
            
            if (optionarr.object(at: 2) as AnyObject).value(forKey: "MCQOptionID") as? String == (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "AnswerID") as? String
            {
                if (optionarr.object(at: 2) as AnyObject).value(forKey: "IsCorrect") as? String == "0" && percentage != "0.0"
                {
                    cell.txtv_optionC.attributedText = htmlC?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 255, green: 38, blue: 0))
                    cell.btnC.titleLabel?.textColor = UIColor.red
                }
            }
            
        }
        else
        {
            cell.optionCheight.constant = 0;
        }
        
        
        if optionarr.count > 3 {
            
            let htmlD = (optionarr.object(at: 3) as AnyObject).value(forKey: "Options") as? String
            
            cell.optionDheight.constant = 50;
            if (optionarr.object(at: 3) as AnyObject).value(forKey: "IsCorrect") as? String == "1"
            {
                cell.txtv_optionD.attributedText = htmlD?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 144, blue: 81))
                cell.btnD.titleLabel?.textColor = UIColor.green
                cell.btnoptionDimg.setImage(UIImage(named: "correct"), for: .normal)
                cell.btnoptionDimg.isHidden = false
            }
            else
            {
                cell.txtv_optionD.attributedText = htmlD?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 0, green: 0, blue: 0))
                cell.btnD.titleLabel?.textColor = UIColor.black
                cell.btnoptionDimg.isHidden = true
            }
            
            if (optionarr.object(at: 3) as AnyObject).value(forKey: "MCQOptionID") as? String == (SummaryArr.object(at: indexPath.row) as AnyObject).value(forKey: "AnswerID") as? String
            {
                if (optionarr.object(at: 3) as AnyObject).value(forKey: "IsCorrect") as? String == "0" && percentage != "0.0"
                {
                    cell.txtv_optionD.attributedText = htmlD?.htmlAttributed(family: "Helvetica", size: 12, color: UIColor.init(red: 255, green: 38, blue: 0))
                    cell.btnD.titleLabel?.textColor = UIColor.red
                }
            }
        }
        else
        {
            cell.optionDheight.constant = 0;
        }
        
        if isfrom == "planner" && percentage == "0.0"
        {
            
        }
        
        
        cell.layoutIfNeeded()
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
    }

    func setdata()  {
        
       lblname.text = Name
       // let BatchName = attended_list[indexPath.row]["BatchName"] as? String
     //   cell.lblTitle.text = String(format: "%@ ( %@ )",StudentName ?? "", BatchName ?? "")
        
        let TotalRight =  TestDetailDict.value(forKey: "TotalRight") as? String
        let TotalWrong =  TestDetailDict.value(forKey: "TotalWrong") as? String
        let TotalQuestion =  TestDetailDict.value(forKey: "TotalQuestion") as? String
        let TotalAttempt =  TestDetailDict.value(forKey: "TotalAttempt") as? String
        let notanswered:Int = (Int(TotalQuestion!) ?? 0) - (Int(TotalAttempt!) ?? 0)
        
        lblcorrect.text = String(format: "%@  -  %@",TotalRight ?? "", TotalQuestion ?? "")
        lblincorrect.text = String(format: "%@  -  %@",TotalWrong ?? "", TotalQuestion ?? "")
        lblnotattempt.text = String(format: "%d  -  %@",notanswered, TotalQuestion ?? "")
        
         let AvgTime =  TestDetailDict.value(forKey: "TakenTime") as! String
        
        self.lbltimetaken?.text = self.stringFromTimeInterval(interval: Double(AvgTime)!/1000 )
        
    }
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let hours = (interval / 3600)
        
        if (hours > 0) {
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        } else {
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }

    func Student_summary()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PlannerID" : PlannerID ,
                              "StudentID" : StudentID,
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_STUDENTSUMMARY)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                       
                        
                        if resData["status"] as? Bool ?? false
                        {
                           
                           self.data = resData["data"] as? [String:Any] ?? [:]
                            
                            let testdetails = self.data["test_details"] as? NSDictionary
                            self.TestDetailDict = testdetails?.mutableCopy() as! NSMutableDictionary
                            print("test_details:",self.TestDetailDict)
                            
                            let appeararr:NSMutableArray = (self.data["appeared_questions"] as! NSArray).mutableCopy() as! NSMutableArray
                            let notappeararr:NSMutableArray = (self.data["not_appeared_questions"] as! NSArray).mutableCopy() as! NSMutableArray
                            
                            if notappeararr.count > 0
                            {
                                let combine = appeararr.adding(notappeararr.object(at: 0))
                                
                                self.SummaryArr = (combine as NSArray ).mutableCopy() as! NSMutableArray
                            }
                            else
                            {
                                let combine = appeararr
                                
                                self.SummaryArr = (combine as NSArray ).mutableCopy() as! NSMutableArray
                            }
                            
                            self.tbllist.reloadData()
                            print("appeared_questions:",self.SummaryArr)
                            self.setdata()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                       
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    
    func Student_Planner()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let parameters = ["PlannerID" : PlannerID ,
                              "StudentMCQTestID" : StudentMCQTestID,
                ] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_PLANNERDETAILS)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        
                        
                        if resData["status"] as? Bool ?? false
                        {
                            
                            self.data = resData["data"] as? [String:Any] ?? [:]
                            
                            let testdetails = resData["data"] as? NSArray
//                            self.TestDetailDict = testdetails?.mutableCopy() as! NSMutableDictionary
//                            print("test_details:",self.TestDetailDict)
//
//                            let appeararr:NSMutableArray = (self.data["appeared_questions"] as! NSArray).mutableCopy() as! NSMutableArray
//                            let notappeararr:NSMutableArray = (self.data["not_appeared_questions"] as! NSArray).mutableCopy() as! NSMutableArray
//
//                            let combine = appeararr.adding(notappeararr.object(at: 0))
                            
                            self.SummaryArr = testdetails?.mutableCopy() as! NSMutableArray
                            self.tbllist.reloadData()
                            print("appeared_questions:",self.SummaryArr)
                            //self.setdata()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }

}
