//
//  chartview.swift
//  TeacherApp
//
//  Created by My Mac on 24/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Charts

class chartview: UIViewController, UITableViewDataSource,UITableViewDelegate,ChartViewDelegate {
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var lbltitle: UILabel!
    
    @IBOutlet var tblheight: NSLayoutConstraint!
    @IBOutlet var charheight: NSLayoutConstraint!
    @IBOutlet weak var topvw: UIView!
    @IBOutlet weak var lblradius: UILabel!
    @IBOutlet weak var lblsubject: UILabel!
    @IBOutlet weak var lblpercentage: UILabel!
    @IBOutlet weak var proggressbar: UIProgressView!
    
    @IBOutlet var chartview: HorizontalBarChartView!
    
    @IBOutlet weak var tbllist: UITableView!
    var MainDict:NSDictionary = [:]
    var PlannerArr:NSArray = []
    var SubjectArr:NSArray = []

    var student_id = ""
    var finishedLoadingInitialTableCells = false
    var strtitle:String = ""
    @IBAction func back_click(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
       // self.setup(barLineChartView: chartview)
        
        chartview.delegate = self
        
        chartview.drawBarShadowEnabled = false
        chartview.drawValueAboveBarEnabled = true
        
        
        
       
        
         imgBlur.setGredient()
        lbltitle.text = strtitle
        topvw.roundCorners(corners: [.topLeft,.topRight,], radius: 10)
        StudentDetails()
        
    }
   
    func StudentDetails()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
              let parameters = ["StudentID" : student_id] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_STUDENTDETAIL)!,
                              method: .post,
                              parameters: parameters,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : NSDictionary = [:]
                        guard let data = response.result.value as? NSDictionary,
                            let _ = data.value(forKey: "status") as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.MainDict = resData.value(forKey: "data") as! NSDictionary
                            self.PlannerArr = self.MainDict.value(forKey: "PlannerPerformance") as! NSArray
                            self.SubjectArr = self.MainDict.value(forKey: "SubjectPerformance") as! NSArray
                            self.lblradius.setRadius(radius:self.lblradius.frame.height/2)
                            self.lblradius.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
                            self.lblradius.text = String(self.MainDict.value(forKey: "Performance") as! Double) + "%"

//                            print("DATA:%@", self.MainDict)
//                            self.MainArr = self.MainArr.adding(self.MainDict) as NSArray
//                            let formatter = NumberFormatter()
//                            formatter.numberStyle = .decimal
//                            formatter.maximumFractionDigits = 2
//                            let str =  formatter.string(from: self.MainDict.value(forKey: "TotalAccuracy") as! NSNumber)
//                            self.lblaccuracy.text = str! + "%"
                            
                           
                            
                           // self.setChart(dataPoints: self.SubjectArr.value(forKey: "SubjectName") as! [String], values: self.SubjectArr.value(forKey: "Performance") as! [Double])
//                            for i in (0..<self.SubjectArr.count)
//                            {
//                                let x = ((self.SubjectArr.object(at: i)) as AnyObject).value(forKey: "Performance") as! String
//                                let y = ((self.SubjectArr.object(at: i)) as AnyObject).value(forKey: "SubjectName") as! String
//                                self.setDataCount(Int(x)! + 1, range: UInt32(y)!)
//                            }
                          //  self.drawPerformanceChart()
                              self.tbllist.reloadData()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    func drawPerformanceChart() {
        
//        let values = [1000, 2000, 3000, 5000, 7000, 8000, 15000, 21000, 22000, 35000]
//        let labels = ["Blue Yonder Airlines", "Aaron Fitz Electrical", "Central Communications", "Magnificent Office Images", "Breakthrough Telemarke", "Lawrence Telemarketing", "Vancouver Resort Hotels", "Mahler State University", "Astor Suites", "Plaza One"]
        
        let labels = SubjectArr.value(forKey: "SubjectName") as! [String]
        let values = SubjectArr.value(forKey: "Performance")  as! [String]
        var dataEntries = [ChartDataEntry]()
        
        for i in 0..<values.count {
            let entry = BarChartDataEntry(x: Double(i), y: Double(values[i]) as! Double)
            
            dataEntries.append(entry)
        }
        
        let barChartDataSet = BarChartDataSet(values: dataEntries, label: "")
        barChartDataSet.drawValuesEnabled = false
        barChartDataSet.colors = ChartColorTemplates.joyful()
        
        let barChartData = BarChartData(dataSet: barChartDataSet)
        chartview.data = barChartData
        chartview.legend.enabled = false
        
        chartview.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        chartview.xAxis.granularityEnabled = true
        chartview.xAxis.granularity = 1
        
        self.chartview.xAxis.labelPosition = .bottom
        chartview.chartDescription?.text = "Performance Chart"
        
        
        chartview.zoom(scaleX: 1.1, scaleY: 1.0, x: 0, y: 0)
        
        let rightAxis = chartview.rightAxis
        rightAxis.drawGridLinesEnabled = false
         rightAxis.drawAxisLineEnabled = true
        
        let leftAxis = chartview.leftAxis
        leftAxis.drawGridLinesEnabled = true
        leftAxis.drawAxisLineEnabled = true
        
        let xAxis = chartview.xAxis
        xAxis.drawGridLinesEnabled = false
        xAxis.drawAxisLineEnabled = true
        chartview.setVisibleXRange(minXRange: 8.0, maxXRange: 8.0)
        
        chartview.setExtraOffsets (left: 0, top: 20.0, right:0.0, bottom: 20.0)
        chartview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInOutBounce)
        chartview.fitBars = true
        //charheight.constant = CGFloat(labels.count * 30)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return PlannerArr.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! progresscell
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5
        cell.layer.borderWidth = 1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderColor = UIColor.lightGray.cgColor
     
        let PlannerName = ((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as! String
        let Performance = ((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "Performance") as! Double
        
            cell.lblname.text = PlannerName
        
            cell.lblpercentage.text = String(Performance) + "%"
            
            cell.proggressview.progress = Float(Performance) / 100
      tblheight.constant = tbllist.contentSize.height
        
        return cell
    }
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }
     let activities = ["Burger", "Steak", "Salad", "Pasta", "Pizza"]
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuestionAnswerView") as! QuestionAnswerView
        nextViewController.PlannerID =  ((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "MCQPlannerID") as! String
        nextViewController.StudentMCQTestID =  ((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "StudentMCQTestID") as! String
        nextViewController.Name =  ((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as! String
        nextViewController.isfrom = "planner"
        nextViewController.percentage = String(((PlannerArr.object(at: indexPath.row)) as AnyObject).value(forKey: "Performance") as! Double)
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
}




extension chartview: IAxisValueFormatter {
//    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
//        return [SubjectArr.value(forKey: "SubjectName") as! String][Int(value) % SubjectArr.count]
//    }
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return activities[Int(value) % activities.count]
    }
}
