//
//  progresscell.swift
//  TeacherApp
//
//  Created by My Mac on 24/04/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit

class progresscell: UITableViewCell {

    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblpercentage: UILabel!
    @IBOutlet weak var proggressview: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
