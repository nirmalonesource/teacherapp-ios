//
//  reportViewController.swift
//  TeacherApp
//
//  Created by My Mac on 06/02/19.
//  Copyright © 2019 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class reportViewController: UIViewController, UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var imgBlur: UIImageView!
    @IBOutlet weak var lblaccuracy: UILabel!
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var tblheight: NSLayoutConstraint!
    
    var MainArr:NSArray = []
   // var ListArr:NSArray = []
    var MainDict:NSDictionary = [:]

    var finishedLoadingInitialTableCells = false

    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlur.setGredient()
        topview.roundCorners(corners: [.topLeft,.topRight], radius: 10)
        self.tbllist.sectionHeaderHeight = 40
        tbllist.tableFooterView = UIView()
        TeacherReport()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tblheight.constant = tbllist.contentSize.height + 50
        
        addTab(strView: "Report")
    }

    @IBAction func MenuActionPressed(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
        self.addChild(popOverVC)
        let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = sd
        //self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
    
    func TeacherReport()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
          //  let parameters = ["MCQPlannerID" : MCQPlannerID ,"BatchID" : BatchID,] as [String : Any]
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_TEACHERREPORT)!,
                              method: .post,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : NSDictionary = [:]
                        guard let data = response.result.value as? NSDictionary,
                            let _ = data.value(forKey: "status") as? Bool
                            else{
                                print("Malformed data received from service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                   
                        
                        if resData["status"] as? Bool ?? false
                        {
                            self.MainDict = resData.value(forKey: "data") as! NSDictionary
                            print("DATA:%@", self.MainDict)
                            self.MainArr = self.MainArr.adding(self.MainDict) as NSArray
                            let formatter = NumberFormatter()
                            formatter.numberStyle = .decimal
                            formatter.maximumFractionDigits = 2
                            let str =  formatter.string(from: self.MainDict.value(forKey: "TotalAccuracy") as! NSNumber)
                            self.lblaccuracy.text = str! + "%"
                            self.tbllist.reloadData()
                        }
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 1:
            return "Assignment Pending"
        case 2:
            return "Assignment Future"
        case 3:
            return "Assignment Missed"
        case 4:
            return "Student Report"
            
        case 5 : return "All Assignments"
        
        default:
            return "Section \(section)"

        }
    }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let rect = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40)
        let imgrect = CGRect(x: tableView.frame.size.width-25, y: 12, width: 15, height: 15)

        let vw = UIView(frame: rect)
        vw.layer.borderWidth = 0.5
        vw.layer.borderColor = UIColor.lightGray.cgColor
        let img = UIImageView(frame: imgrect)
        img.image = UIImage(named: "arrow")
        //vw.backgroundColor = UIColor.lightGray
        let lbl = UILabel()
        lbl.frame = vw.frame
        lbl.textColor = #colorLiteral(red: 0.1456475525, green: 0.3257931114, blue: 0.2144839528, alpha: 1)
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        let btn = UIButton(frame: rect)
        btn.tag = section
        btn.addTarget(self, action:#selector(buttonClicked), for: .touchUpInside)
        vw.addSubview(img)
        vw.addSubview(lbl)
        vw.addSubview(btn)

        switch section {
        case 0:
            lbl.text = "  Pending Assignment"
        case 1:
            lbl.text = "  Next Assignment"
        case 2:
            lbl.text = "  Missed Assignment"
        case 3:
            lbl.text = "Student Report"
        
        case 4 :
            lbl.text = "All Assignments"
            
        default:
            lbl.text = "Section \(section)"
        }
        return vw
    }
    @objc func buttonClicked(sender:UIButton)
    {
        print("TAG:%@",sender.tag)
        if(sender.tag == 0){
            if (MainDict.value(forKey: "AssignmentPending") as! NSArray).count > 0
            {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailedreportview") as! detailedreportview
                nextViewController.strtitle = "Pending Assignment"
                nextViewController.MainArr =  MainDict.value(forKey: "AssignmentPending") as! NSArray
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
               showToast(uiview: self, msg:"Pending Assignment Not Available")
            }
            
        }
        if(sender.tag == 1){
            if (MainDict.value(forKey: "AssignmentFuture") as! NSArray).count > 0
            {
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailedreportview") as! detailedreportview
            nextViewController.strtitle = "Next Assignment"
            nextViewController.MainArr =  MainDict.value(forKey: "AssignmentFuture") as! NSArray
            self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                showToast(uiview: self, msg:"Next Assignment Not Available")
            }
        }
        if(sender.tag == 2){
            if (MainDict.value(forKey: "AssignmentMissed") as! NSArray).count > 0
            {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailedreportview") as! detailedreportview
                nextViewController.strtitle = "Missed Assignment"
                nextViewController.MainArr =  MainDict.value(forKey: "AssignmentMissed") as! NSArray
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                showToast(uiview: self, msg:"Missed Assignment Not Available")
            }
            
        }
        if(sender.tag == 3){
            if (MainDict.value(forKey: "StudentInfo") as! NSArray).count > 0
            {
                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "detailedreportview") as! detailedreportview
                nextViewController.strtitle = "Student Report"
                nextViewController.MainArr =  MainDict.value(forKey: "StudentInfo") as! NSArray
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            
        }
        if(sender.tag == 4){
                 
                       let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "AllAssignmentViewController") as!  AllAssignmentViewController
                       self.navigationController?.pushViewController(nextViewController, animated: true)
                   }
                   
               
       
        print("buttonClicked")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if MainArr.count > 0
        {
            switch section {
            case 0:
                return (MainDict.value(forKey: "AssignmentPending") as! NSArray).count
            case 1:
                return (MainDict.value(forKey: "AssignmentFuture") as! NSArray).count
            case 2:
                return (MainDict.value(forKey: "AssignmentMissed") as! NSArray).count
//            case 3:
//                return (MainDict.value(forKey: "StudentInfo") as! NSArray).count
                case 2:
                return 1
            default:
                return 0
                
            }
        }
       return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! newreportcell
        let AssignmentPending:NSArray = MainDict.value(forKey: "AssignmentPending") as! NSArray
        let AssignmentFuture:NSArray = MainDict.value(forKey: "AssignmentFuture") as! NSArray
        let AssignmentMissed:NSArray = MainDict.value(forKey: "AssignmentMissed") as! NSArray
        
        
      //  let StudentInfo:NSArray = MainDict.value(forKey: "StudentInfo") as! NSArray

        switch indexPath.section {
        case 0:
            cell.lblname.text = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as? String
            
            let plnrname = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "SubjectName") as? String
            let StandardName = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "StandardName") as? String
            let MediumName = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "MediumName") as? String
            let BoardName = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "BoardName") as? String
            let part1 = plnrname! + " | " + StandardName!
            let part2 = part1 + " | " + MediumName!
            let part3 = part2 + ".(" + BoardName! + ")"
             cell.lblsubject.text = part3
            
            let StartDate = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "StartDate") as? String
            let EndDate = ((AssignmentPending.object(at: indexPath.row)) as AnyObject).value(forKey: "EndDate") as? String

            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            
            if let date = dateFormatterGet.date(from: StartDate!) {
                cell.lblstartdate.text = "Start Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            if let date = dateFormatterGet.date(from: EndDate!) {
                cell.lblenddate.text = "End Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            
        case 1:
            cell.lblname.text = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as? String
            let plnrname = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "SubjectName") as? String
            let StandardName = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "StandardName") as? String
            let MediumName = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "MediumName") as? String
            let BoardName = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "BoardName") as? String
            let part1 = plnrname! + " | " + StandardName!
            let part2 = part1 + " | " + MediumName!
            let part3 = part2 + ".(" + BoardName! + ")"
            cell.lblsubject.text = part3
            
            let StartDate = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "StartDate") as? String
            let EndDate = ((AssignmentFuture.object(at: indexPath.row)) as AnyObject).value(forKey: "EndDate") as? String
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            
            if let date = dateFormatterGet.date(from: StartDate!) {
                cell.lblstartdate.text = "Start Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            if let date = dateFormatterGet.date(from: EndDate!) {
                cell.lblenddate.text = "End Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }

        case 2:
            cell.lblname.text = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as? String
            let plnrname = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "SubjectName") as? String
            let StandardName = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "StandardName") as? String
            let MediumName = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "MediumName") as? String
            let BoardName = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "BoardName") as? String
            let part1 = plnrname! + " | " + StandardName!
            let part2 = part1 + " | " + MediumName!
            let part3 = part2 + ".(" + BoardName! + ")"
            cell.lblsubject.text = part3
            
            let StartDate = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "StartDate") as? String
            let EndDate = ((AssignmentMissed.object(at: indexPath.row)) as AnyObject).value(forKey: "EndDate") as? String
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"
            
            if let date = dateFormatterGet.date(from: StartDate!) {
                cell.lblstartdate.text = "Start Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
            if let date = dateFormatterGet.date(from: EndDate!) {
                cell.lblenddate.text = "End Date: " + dateFormatterPrint.string(from: date)
                print(dateFormatterPrint.string(from: date))
            }
//        case 3:
//            cell.lblname.text = ((StudentInfo.object(at: indexPath.row)) as AnyObject).value(forKey: "PlannerName") as? String

        default:
            return cell
            
        }
        
        tblheight.constant = tableView.contentSize.height + 50;
        
        return cell
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        //    return contentHeights[indexPath.row]
    //        return UITableView.automaticDimension
    //
    //        //        if contentHeights[indexPath.row] != 0.0 {
    //        //            print("MYHEIGHT:",contentHeights[indexPath.row])
    //        //            tblheightconstant.constant = tblquestions.contentSize.height
    //        //            return contentHeights[indexPath.row]
    //        //        }
    //        //        return 0
    //    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        var lastInitialDisplayableCell = false
        if 1 > 0 && !finishedLoadingInitialTableCells {
            if let indexPathsForVisibleRows = tableView.indexPathsForVisibleRows,
                let lastIndexPath = indexPathsForVisibleRows.last, lastIndexPath.row == indexPath.row {
                lastInitialDisplayableCell = true
            }
        }
        if !finishedLoadingInitialTableCells {
            if lastInitialDisplayableCell {
                finishedLoadingInitialTableCells = true
            }
            cell.loadAnimation()
        }
    }

}
