//
//  AllAssignmentViewController.swift
//  TeacherApp
//
//  Created by Mac on 02/04/20.
//  Copyright © 2020 My Mac. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import ActionSheetPicker_3_0
import SwiftyPickerPopover


class ALLAssignmentcell: UITableViewCell {
   
    @IBOutlet weak var imgIcon: UIImageView!
       @IBOutlet weak var lblPaperTitle: UILabel!
       @IBOutlet weak var lblTime: UILabel!
       @IBOutlet weak var lblSubTitle: UILabel!
       @IBOutlet weak var lblLocation: UILabel!
       
    @IBOutlet weak var lblpendingstudents: UILabel!
    
    @IBOutlet weak var lblcompletedstudents: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

class AllAssignmentViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
    
    
    
       @IBOutlet weak var btnSelectDate: UIButton!
       @IBOutlet weak var btnSelectStandard: UIButton!
       @IBOutlet weak var btnSelectBatch: UIButton!
       
       @IBOutlet weak var btnReset: UIButton!
       @IBOutlet weak var btnSave: UIButton!
       
       @IBOutlet weak var imgSaveBG: UIImageView!
       var finishedLoadingInitialTableCells = false
    
       var subjectList = [[String : Any]]()
       var selectedStandardIndex = Int()
       var selectedStandardID = String()
       var selectedBatchID = String()
       var strSelectedDate = String()
    
    
    @IBOutlet weak var vwFilter: UIView!
    
    @IBOutlet weak var imgpopup: UIImageView!
    @IBOutlet weak var imgblur: UIImageView!
    
    @IBOutlet weak var topview: UIView!
    
    @IBOutlet weak var viewCount: UIView!
    
    @IBOutlet weak var lblassignmentcount: UILabel!
    
    @IBOutlet weak var lblstudentcount: UILabel!
    
      var paperList = [[String : Any]]()
      var isfilterclicked = Bool()
    
    @IBOutlet weak var tblAllassignment: UITableView!
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paperList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ALLAssignmentcell = tableView.dequeueReusableCell(withIdentifier: "ALLAssignmentcell") as!  ALLAssignmentcell
        
        cell.lblPaperTitle.text = paperList[indexPath.row]["PlannerName"] as? String
               
              // cell.lblTime.text = paperList[indexPath.row]["EndDate"] as? String
               
            
           
               cell.lblSubTitle.text = "\(paperList[indexPath.row]["SubjectName"] as? String ?? "") | " + "\(paperList[indexPath.row]["StandardName"] as? String ?? "") | " + "\(paperList[indexPath.row]["MediumName"] as? String ?? "")" + "(\(paperList[indexPath.row]["BoardName"] as? String ?? ""))"

        cell.lblpendingstudents.text = "\(paperList[indexPath.row]["pending_students"] as? Int ?? 0)"
        cell.lblcompletedstudents.text = "\(paperList[indexPath.row]["total_completed_students"] as? Int ?? 0)"
        
        
        
       cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyPlanViewController") as! MyPlanViewController
              nextViewController.dictData = paperList[indexPath.row]
              self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

      
        
          imgblur.setGredient()
          topview.roundCorners(corners: [.topLeft,.topRight], radius: 10)
         AllAssignment()
          
        
               vwFilter.setRadius(radius: 10)
               btnSelectDate.setRadiusBorder(color: UIColor.lightGray)
               btnSelectBatch.setRadiusBorder(color: UIColor.lightGray)
               btnSelectStandard.setRadiusBorder(color: UIColor.lightGray)
               btnReset.setRadiusBorder(color: UIColor.init(rgb: ConstantVariables.Constants.navigationColor))
               
               btnSelectDate.roundCorners(corners: [.topLeft , .topRight], radius: 10)
               btnSelectBatch.roundCorners(corners: [.topLeft , .topRight], radius: 10)
               btnSelectStandard.roundCorners(corners: [.topLeft , .topRight], radius: 10)

               btnReset.setRadius(radius: 10)
               btnSave.setRadius(radius: 10)
               
               imgSaveBG.setGredient()
               imgSaveBG.setRadius(radius: 10)
        
               self.view.layoutIfNeeded()
               
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
    }

    func AllAssignment()
       {
           if !isInternetAvailable(){
               noInternetConnectionAlert(uiview: self)
           }
           else {
               
               SVProgressHUD.show(withStatus: nil)
               
            let parameters = ["BranchID" : "",
                "BoardID" : "",
                "MediumID" : "",
                "StandardID" : selectedStandardID,
                "BatchID" : selectedBatchID,
                "SubjectID" : "",
                "PaperID" : "",
                "DateMonth" : strSelectedDate
                   ] as [String : Any]
               
            print("allassignmentparams",parameters)
               let username = ServiceList.USERNAME
               let password = ServiceList.PASSWORD
               let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
               let base64LoginData = loginData.base64EncodedString()
               let headers = ["Authorization": "Basic \(base64LoginData)",
                   "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                   "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                   "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
               
               Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.Get_AssignPaperlist)!,
                                 method: .post,
                                 parameters: parameters,
                                 headers: headers).responseJSON
                   { (response:DataResponse) in
                       switch(response.result) {
                       case .success(let data):
                           print(" i got my Data Yup..",data)
                           
                           var resData : [String : AnyObject] = [:]
                           guard let data = response.result.value as? [String:AnyObject],
                               let _ = data["status"]! as? Bool
                               else{
                                   print("Malformed data received from service")
                                   SVProgressHUD.dismiss()
                                   
                                   return
                           }
                           
                           resData = data
                          print(resData)
                           
                           if resData["status"] as? Bool ?? false
                           {
                            
                            self.paperList = resData["data"]!["paper_info"] as? [[String:Any]] ?? []
                            self.lblassignmentcount.text = "Total Assignment: " + "\(resData["data"]!["total_assignment"] as? Int ?? 0)"
                            self.lblstudentcount.text = "Student: " + "\(resData["data"]!["total_students"] as? Int ?? 0)"
                            
                                 
                            print(self.paperList)
                            
                           }
                          else
                            {
                                    showToast(uiview: self, msg: resData["message"] as? String ?? "")
                            }
                           self.tblAllassignment.reloadData()
                           SVProgressHUD.dismiss()
                       case .failure(let error):
                           print(error)
                           SVProgressHUD.dismiss()
                       }
               }
           }
       }
    
    func getSubjectList()
      {
          if !isInternetAvailable(){
              noInternetConnectionAlert(uiview: self)
          }
              
          else {
              
              SVProgressHUD.show(withStatus: nil)
              
              let username = ServiceList.USERNAME
              let password = ServiceList.PASSWORD
              let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
              let base64LoginData = loginData.base64EncodedString()
              let headers = ["Authorization": "Basic \(base64LoginData)",
                  "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                  "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                  "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
              
              Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.GET_SUBJECT_LIST)!,
                                method: .get,
                                parameters: nil,
                                headers: headers).responseJSON
                  { (response:DataResponse) in
                      switch(response.result) {
                      case .success(let data):
                          print(" i got my Data Yup..",data)
                          
                          var resData : [String : AnyObject] = [:]
                          guard let data = response.result.value as? [String:AnyObject],
                              let _ = data["status"]! as? Bool
                              else{
                                  print("Malformed data received from fetchAllRooms service")
                                  SVProgressHUD.dismiss()
                                  
                                  return
                          }
                          
                          resData = data
                          self.subjectList.removeAll()
                          if resData["status"] as? Bool ?? false
                          {
                              self.subjectList = resData["data"] as? [[String:Any]] ?? []
                          }
                              
                          else
                          {
                              showToast(uiview: self, msg: resData["message"] as? String ?? "")
                          }
                          
                          self.view.layoutIfNeeded()
                          
                          SVProgressHUD.dismiss()
                      case .failure(let error):
                          print(error)
                          SVProgressHUD.dismiss()
                      }
              }
          }
      }
    
    @IBAction func btn_sort(_ sender: Any) {
        var MainArr : NSArray = paperList as NSArray
           
           if isfilterclicked
           {
               isfilterclicked = false
            let newArr = MainArr.sortedArray(using: [NSSortDescriptor(key: "PlannerName", ascending: false)]) as! NSArray
               MainArr = newArr
            paperList = MainArr as! [[String:Any]]
           }
           else
           {
               isfilterclicked = true
               let newArr = MainArr.sortedArray(using: [NSSortDescriptor(key: "PlannerName", ascending: true)]) as NSArray
               MainArr = newArr
            paperList = MainArr as! [[String:Any]]
           }
           
           tblAllassignment.reloadData()
    
    }
    
    
    @IBAction func btn_filter(_ sender: Any) {
        getSubjectList()
        showViewWithAnimation(vw:vwFilter,img:imgpopup)
        
    }
    
    @IBAction func btn_closefilter(_ sender: Any) {
         hideViewWithAnimation(vw: vwFilter, img:imgpopup)
        
    }
    
    
    @IBAction func btnselectdate(_ sender: UIButton) {
         self.view.endEditing(true)
        //        let datePicker = ActionSheetDatePicker(title: "Select Date:", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
        //            picker, value, index in
        //
        //            let dateFormatterGet = DateFormatter()
        //            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz"
        //            let dateFormatter = DateFormatter()
        //            dateFormatter.dateFormat = "MMM dd , yyyy"
        //
        //            let date: Date? = dateFormatterGet.date(from: "\(value!)")!
        //            print(dateFormatter.string(from: date!))
        //            sender.setTitle("\(dateFormatter.string(from: date!))", for: .normal)
        //
        //            let dateFormatter1 = DateFormatter()
        //            dateFormatter1.dateFormat = "yyyy/MM/dd"
        //
        //            let date1: Date? = dateFormatterGet.date(from: "\(value!)")!
        //            self.strSelectedDate = (dateFormatter1.string(from: date1!))
        //            print("Selected : \(self.strSelectedDate)")
        //            return
        //        }, cancel: { ActionStringCancelBlock in return },
        //           origin: sender.superview!.superview)
        //        datePicker?.maximumDate = Date()
        //        datePicker?.show()
                
                DatePickerPopover(title: "Select Month")
                        .setDateMode(.date)
                        //.setMaximumDate(Date())
                        .setSelectedDate(Date())
                        .setDoneButton(action: {
                            popover, selectedDate in
                            
                            print("selectedDate \(selectedDate)")
                            let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy-MM"
                           sender.setTitle("\(formatter.string(from: selectedDate))", for: .normal)
                          
                           let dateFormatter1 = DateFormatter()
                              dateFormatter1.dateFormat = "yyyy-MM"
                          self.strSelectedDate = (dateFormatter1.string(from: selectedDate))
                              print("Selected : \(self.strSelectedDate)")
                        })
                        .setCancelButton(action: { _, _ in print("cancel")}).appear(originView:sender, baseViewController: self)
    }
    
    
    @IBAction func btnselectStandar(_ sender: UIButton) {
          self.view.endEditing(true)
                if subjectList.count != 0
                {
                    ActionSheetStringPicker.show(withTitle: "Select Standard", rows: subjectList.map({ $0["StandardName"] as? String ?? ""}) as [Any], initialSelection: 0, doneBlock: {
                        picker, indexes, values in
                        self.btnSelectStandard.setTitle("\(values!)", for: .normal)
                        self.selectedStandardID = self.subjectList[indexes]["StandardID"] as? String ?? ""
  
                        return
                    }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
                }
    }
    
    
    @IBAction func btnSelectBatch(_ sender: UIButton) {
    self.view.endEditing(true)
          
        let Batch : NSArray  = (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey:"batch") as? NSArray ?? []
        
         //let arr:NSArray = batch_list as NSArray
                
                
        
        ActionSheetStringPicker.show(withTitle: "BATCH", rows:Batch.value(forKey: "BatchName") as? [Any], initialSelection: 0, doneBlock: {
                     picker, indexes, values in
                      SVProgressHUD.show(withStatus: nil)
                  self.btnSelectBatch.setTitle("\(values!)", for: .normal)
            self.selectedBatchID =  (Batch.object(at: indexes) as AnyObject).value(forKey: "BatchID") as! String
            print(self.selectedBatchID)
            
            
            
                     return
                 }, cancel: { ActionMultipleStringCancelBlock in
                     print("CANCELCLICK")
                    self.selectedBatchID = ""
                      SVProgressHUD.show(withStatus: nil)
                     return }, origin: sender)
                 
    }
    
    
    @IBAction func btnreset(_ sender: Any) {
    
         btnSelectStandard.setTitle("Select Standard", for: .normal)
          selectedStandardID = ""
          
          btnSelectBatch.setTitle("Select Batch", for: .normal)
          selectedBatchID = ""
          
          btnSelectDate.setTitle("Select Date", for: .normal)
          strSelectedDate = ""
        
    }
    
    @IBAction func btnsave(_ sender: Any) {
        AllAssignment()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
