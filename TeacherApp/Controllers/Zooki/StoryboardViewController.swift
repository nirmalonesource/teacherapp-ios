//
//  Created by Pete Smith
//  http://www.petethedeveloper.com
//
//
//  License
//  Copyright © 2017-present Pete Smith
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

import UIKit
import ScalingCarousel
import Alamofire
import SVProgressHUD

class Cell: ScalingCarouselCell {
    
    @IBOutlet weak var imgVw: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}

class StoryboardViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var carousel: ScalingCarouselView!
    @IBOutlet weak var lblCurrentSelection: UILabel!
    @IBOutlet weak var imgBG: UIImageView!
    
    var zookiList = [[String : Any]]()
    var folder_path = String()
    
    //    @IBOutlet weak var carouselBottomConstraint: NSLayoutConstraint!
//    @IBOutlet weak var output: UILabel!
    
    private struct Constants {
        static let carouselHideConstant: CGFloat = -250
        static let carouselShowConstant: CGFloat = 15
    }
    
    //MARK:- Web service
    
    func getZookiList()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
            
        else {
            
            SVProgressHUD.show(withStatus: nil)
            
            let username = ServiceList.USERNAME
            let password = ServiceList.PASSWORD
            let loginData = String(format: "%@:%@", username, password).data(using: String.Encoding.utf8)!
            let base64LoginData = loginData.base64EncodedString()
            let headers = ["Authorization": "Basic \(base64LoginData)",
                "X-CI-CHILDRENS-ACADEMY-API-KEY" : ServiceList.X_CI_CHILDRENS_ACADEMY_API_KEY,
                "User-Id" : (UserDefaults.standard.getUserDict()["teacher"] as AnyObject).value(forKey: "id") as? String ?? "",
                "X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN" : UserDefaults.standard.getUserDict()["login_token"] as? String ?? ""]
            
            Alamofire.request(URL(string: ServiceList.SERVICE_URL+ServiceList.ZOOKI_LIST)!,
                              method: .get,
                              parameters: nil,
                              headers: headers).responseJSON
                { (response:DataResponse) in
                    switch(response.result) {
                    case .success(let data):
                        print(" i got my Data Yup..",data)
                        
                        var resData : [String : AnyObject] = [:]
                        guard let data = response.result.value as? [String:AnyObject],
                            let _ = data["status"]! as? Bool
                            else{
                                print("Malformed data received from fetchAllRooms service")
                                SVProgressHUD.dismiss()
                                
                                return
                        }
                        
                        resData = data
                        self.zookiList.removeAll()
                        if resData["status"] as? Bool ?? false
                        {
                            self.zookiList = resData["data"] as? [[String:Any]] ?? []
                            self.folder_path = resData["folder_path"] as? String ?? ""
                        }
                            
                        else
                        {
                            showToast(uiview: self, msg: resData["message"] as? String ?? "")
                        }
                        
                    self.lblCurrentSelection.text = String(describing: 1) + "/\(self.zookiList.count)"
                        self.carousel.reloadData()
                        self.view.layoutIfNeeded()
                        
                        SVProgressHUD.dismiss()
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.dismiss()
                    }
            }
        }
    }
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBG.setGredient()
        getZookiList()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        addTab(strView: "Zooki")
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        carousel.deviceRotated()
    }
    
    // MARK: - Button Actions
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        let popOverVC = self.storyboard?.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
        
        self.addChild(popOverVC)
        let sd  = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        popOverVC.view.frame = sd
        //self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        // self.view.bringSubviewToFront(popOverVC.view)
        UIApplication.shared.keyWindow!.bringSubviewToFront(popOverVC.view)
    }
    
    @IBAction func showHideButtonPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

typealias CarouselDatasource = StoryboardViewController
extension CarouselDatasource: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return zookiList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! Cell

        if let scalingCell = cell as? ScalingCarouselCell {
            scalingCell.mainView.backgroundColor = .white
            scalingCell.mainView.clipsToBounds = true
        }

        let folderath = folder_path + "\(zookiList[indexPath.row]["Image"] as? String ?? "")"
        cell.imgVw.sd_setImage(with: URL(string : (folderath.URLQueryAllowedString()!)), placeholderImage : nil , options: .progressiveDownload,
                               completed: nil)
        /*
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        
        if zookiList[indexPath.row]["CreatedDate"] as? String ?? "" !=  "0000-00-00 00:00:00" && zookiList[indexPath.row]["CreatedDate"] as? String ?? "" != ""
        {
             let date: Date? = dateFormatter.date(from: zookiList[indexPath.row]["CreatedDate"] as? String ?? "")!
           
            let a = dateFormatter.string(from: date!)
            
            cell.lblDate.text = a
            
        }
        */
        
        let df = zookiList[indexPath.row]["CreatedDate"] as? String ?? ""
        let getDate = setDateWithFormat(strDate: df)
        cell.lblDate.text = getDate

        
        cell.lblTitle.text = zookiList[indexPath.row]["Title"] as? String
        cell.lblDescription.text = zookiList[indexPath.row]["Description"] as? String
        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        
        return cell
    }
    
    
    func setDateWithFormat(strDate : String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd ,yyyy"
        //"MMM dd ,yyyy HH:mm"
        let date: Date? = dateFormatterGet.date(from: strDate)!
        return dateFormatter.string(from: date!)
    }
}

/*
 let df = search_list_Array[indexPath.row]["SubmitDate"] as? String ?? ""
 let getDate = setDateWithFormat(strDate: df)
 cell.lblDate.text = getDate
 */

typealias CarouselDelegate = StoryboardViewController
extension StoryboardViewController: UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //carousel.didScroll()
        
        guard let currentCenterIndex = carousel.currentCenterCellIndex?.row else { return }
        
        lblCurrentSelection.text = String(describing: currentCenterIndex + 1) + "/\(zookiList.count)"
    }
}

private typealias ScalingCarouselFlowDelegate = StoryboardViewController
extension ScalingCarouselFlowDelegate: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
}

